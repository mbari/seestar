![SeeStar Logo](https://bitbucket.org/mbari/seestar/raw/master/images/seestar_logo_med.png)
#SeeStar 
##An open-source underwater time-lapse imaging system

###Background
There are many systems, both software and instrumentation, that have been developed in an attempt to automate quantification of plankton from images. SeeStar is a modular underwater camera system designed to operate autonomously on platforms of opportunity, including piers, mooring cables, ships, and mobile vehicles. Its goals are to leverage off-the-shelf and inexpensive technology to create a widely accessible system.

![SeeStar prototype](https://bitbucket.org/mbari/seestar/raw/master/images/SeeStar_II_Labeled.jpg "SeeStar II with optional deep-rated housings")


##SeeStar II - Pre-release info
January 2016: The second generation SeeStar system is released. This Arduino-controlled system is easier to build than the first generation. There are also options for purchasing deep-rated (1500m) housings that are SeeStar compatible. 

SeeStar II - The README and all the files are in the [Source folder](https://bitbucket.org/mbari/seestar/src/master/SeeStar_II_Release/?at=master). You can download all the files at once in the [SeeStar II zip archive](https://bitbucket.org/mbari/seestar/src/master/SeeStar_II_Release.zip?at=master)


##Contact info

Contact either `ckecy` or `haddock` at `mbari{dot}org` or post an issue here.

-----
![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA license")

This work is distributed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) license.
