#SeeStar II Imaging System Initial Document 
Release  Jan 19, 2016
----------------------------------------------------------------------
###MBARI team:
Steven Haddock, Ph.D., Principal Investigator
Chad Kecy (Electrical Engineer), Project Manager
Francois Cazenave (Mechanical Engineer)
Thom Maughan (Software Engineer)
Danelle Cline (Software Engineer)


##Open Source License
----------------------------------------------------------------------
The SeeStar II Imaging System is an open source project and is covered by the Creative Commons Attribution-ShareAlike 4.0 license. See the Open_Source_License.txt file for details.



##Installation Notes
----------------------------------------------------------------------

When unzipped, this file will contain all of the files needed to build and test a SeeStar II Imaging System. The following folder structure will be generated:


* SeeStar_II_Release
    * Documents
        * Manufacturers_Datasheets
        * Operation
        * Reference
        * Technical_Manuals
    * Drawings
        * Electrical
        * Mechanical



##Files Overview
--------------------------------------------------------------------

* SeeStar_II_Release/Documents/Manufacturers_Datasheets
	*	Contains datasheets and app notes from subassemblies

* SeeStar_II_Release/Documents/Operation
	*	SeeStar_II_Operations_Manual - A top level guide to operating SeeStar
	*	SeeStar_II_Programming_Guide - A comprehensive guide to programming SeeStar

* SeeStar_II_Release/Documents/Reference
	* SeeStar_II_Arduino_Controller_Shield_Hardware_Interface - Technical details of the controller shield
	*	SeeStar_II_Battery_Charge_and_Discharge_Tests - Test results of the battery module testing
	*	SeeStar_II_DC-DC_Converter_Efficiency_Test - Test results of the LED module testing
	*	SeeStar_II_System_Bill_of_Material - A system level bill of material

* SeeStar_II_Release/Documents/Technical_Manuals
	*	SeeStar_II_Battery_Module_Technical_Manual - Detailed information on the battery module
	*	SeeStar_II_Camera_Module_Technical_Manual - Detailed information on the camera module
	*	SeeStar_II_LED_Module_Technical_Manual - Detailed information on the LED module

* SeeStar_II_Release/Drawings/Electrical
	*	Contains electrical schematics of the SeeStar II System

* SeeStar_II_Release/Drawings/Mechanical
 	* 	Contains mechanical drawings of the SeeStar II System



##Software
--------------------------------------------------------------------
The Arduino sketch (firmware) is located at https://codebender.cc/sketch:199916




