#SeeStar Arduino Yún GoPro Hero control

## Yún WiFi control of GoPro Hero Camera

##Requirements

* GoPro Hero3 or better camera
* [Arduino Yún board](http://arduino.cc/en/Main/ArduinoBoardYun?from=Products.ArduinoYUN)
* Micro-USB cable (for power)
* Ethernet cable (optional - for faster uploads of Arduino sketches)
* Arduino software version 1.6.3 or better
* Time library
    * http://www.pjrc.com/teensy/td_libs_Time.html#ntp

##Installation
* Install Arduino Software IDE to upload code to your Yún board. Code is freely available [here](http://arduino.cc/en/Main/Software) for Windows, Mac OSX, or Linux
 
* Power on Arduino Yún and connect it to your computer using WiFi. When you first power up the Yún, it will create a WiFi network called `ArduinoYun-XXXXXXXX` where `XXXXXX` is a hex string. Connect to this network.

![wifi connect](https://bitbucket.org/mbari/seestar/raw/master/Build/source/arduino/yun/images/wifi_screen_grab.png )

* In Firefox or Chrome enter the default Arduino board address `192.168.240.1` or `arduino.local`. Be patient - this may take a while. If this fails, try resetting the board.


![connect](https://bitbucket.org/mbari/seestar/raw/master/Build/source/arduino/yun/images/connect.png)

* The default password for Arduino boards is simply `arduino`. Click log in.


![password login](https://bitbucket.org/mbari/seestar/raw/master/Build/source/arduino/yun/images/password_login.png)

* Click **configure**

* In the wireless parameters section, select the GoPro wireless network and enter your GoPro password. Click **configure** and restart. Be patient - this takes a few minutes


![wireless setup](https://bitbucket.org/mbari/seestar/raw/master/Build/source/arduino/yun/images/gopro_yun_wireless_setup.png)

##Reference Documents



##Contact info

Contact either `dcline` or `haddock` or `ckecy` at `mbari{dot}org` or post an issue here.

-----
![CC-BY-SA](http://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA license")

This work is distributed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) license.
