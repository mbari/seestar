/*
  Time Sync 

 This sketch is intended to be run with the MBARI SeeStar time-lapse imaging system
 with a custom configured Arduino Yun board designed to work in a SeeStar design. 
 This is work in progress and expected to be modified significantly in the coming
 months.    
 
 Thie sketch gets the time from a NTP server, sets the time oon the Linux processor 
 via Bridge then parses out year, month, day, hours, minutes and seconds and 
 sets the hero3 camera time based on the Linux processor time

 See the README.md file in TBD
 
 for details on setup.
  
 created by D.Cline 7 April 2015 dcline@mbari.org
 based on the example by Tom Igoe  

 This work is distributed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) license. 
 */


#include <Process.h>
#include <HttpClient.h>

#define TIMELAPSE_INTERVAL_SECONDS 300

Process date;                 // process used to get the date
unsigned int hours, minutes, seconds;  // for the results
unsigned int day, month, year;  // for the results
unsigned long lastLoopTime;
unsigned long timlapseInterval;

void setup() {
  Bridge.begin();        // initialize Bridge
  Serial.begin(9600);    // initialize serial  

  while(!Serial);               // wait for Serial Monitor to open
  Serial.println("Time Check");  // Title of sketch

  // run an initial date process. Should return:
  // hh:mm:ss :
  if (!date.running())  {
    date.begin("date");
    date.addParameter("+%F_%T");
    date.run();
  }
    
  lastLoopTime = 0;
  timlapseInterval = TIMELAPSE_INTERVAL_SECONDS * 1000L;
  
}

void loop() {

  unsigned long curTime = millis();
  
  // Note: the code does not take the processing time into account, 
  //       thus the actual capturing interval slightly deviates from the defined one!
  if (curTime - lastLoopTime >= timlapseInterval) {
    // print the date: 
    Serial.print(year);    
    Serial.print("-");
    Serial.print(month);    
    Serial.print("-");
    Serial.print(day);    
    Serial.print("-");  
    
    // print the ISO separate T
    Serial.print("T");
    
    // print the time:
    if (hours <= 9) Serial.print("0");    // adjust for 0-9
    Serial.print(hours);    
    Serial.print(":");
    if (minutes <= 9) Serial.print("0");  // adjust for 0-9
    Serial.print(minutes);
    Serial.print(":");
    if (seconds <= 9) Serial.print("0");  // adjust for 0-9
    Serial.println(seconds);

    // restart the date process:
    if (!date.running())  {
      date.begin("date");
      date.addParameter("+%F_%T");
      date.run();
    }
  }

  //if there's a result from the date process, parse it:
  if (date.available()>0) {
    // get the result of the date process (should be hh:mm:ss):
    String timeString = date.readString();    

    // find the colons: , dashes- and underscores
    int firstColon = timeString.indexOf(":");
    int secondColon= timeString.lastIndexOf(":");
    int firstDash = timeString.indexOf("-");
    int secondDash= timeString.lastIndexOf("-");
    int underscore = timeString.indexOf("_");

    // get the substrings
    String yearString = timeString.substring(0, firstDash); 
    String monString = timeString.substring(firstDash+1, secondDash);
    String dayString = timeString.substring(secondDash+1, underscore);
    String hourString = timeString.substring(underscore+1, firstColon); 
    String minString = timeString.substring(firstColon+1, secondColon);
    String secString = timeString.substring(secondColon+1);

    // convert to ints,saving the previous second:
    year = yearString.toInt() - 2000;
    month = monString.toInt();
    day = dayString.toInt();
    hours = hourString.toInt();
    minutes = minString.toInt();
    seconds = secString.toInt();
    
    // Initialize the client library
    HttpClient client;
  
    // Format and set camera time
    char format[128];
    sprintf(format, "http://10.5.5.9/camera/TM?t=gopro3fish&p=%%%.2X%%%.2X%%%.2X%%%.2X%%%.2X%%%.2X", year, month, day, hours, minutes, seconds);
       
    Serial.println(format);
    client.get(format);
    if (!client.exitValue()) 
      Serial.print("Time set");
    else
      Serial.print("Error setting time"); 
    lastLoopTime = millis();
  
  }
} 

