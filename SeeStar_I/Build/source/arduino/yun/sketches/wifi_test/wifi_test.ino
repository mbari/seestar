/*
 WiFi test

 This sketch is intended to be run with the MBARI SeeStar time-lapse imaging system
 with a custom configured Arduino Yun board designed to work in a SeeStar design. 
 This is work in progress and expected to be modified significantly in the coming
 months.  

 See the README.md file in TBD
 
 for details on setup.
  
 created by D.Cline 7 April 2015 dcline@mbari.org

 This work is distributed under a [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0) license. 
 */

#include <Process.h>
#include <Bridge.h>
#include <HttpClient.h>
#include <MsTimer2.h>

#define TIMELAPSE_INTERVAL_SECONDS 10
#define CAM_POWER_PIN 13
#define CAM_ON_PIN 12
#define LED_POWER_PIN 11
#define RTC_SS_PIN 10
#define UART_SHDN_PIN 9
#define RTC_RESET_PIN 8

unsigned long _lastLoopTime;
unsigned long _timlapseInterval;
 
void setup() {
    
  // bridge takes about 30-60 seconds to start up
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  Bridge.begin();
  digitalWrite(13, HIGH);

  Serial.begin(9600);

  // wait for a serial connection 
  while (!Serial);   
  Serial.println("Serial port connected.");
  
  // initialize the ports 
  pinMode(CAM_POWER_PIN, OUTPUT);  
  pinMode(LED_POWER_PIN, OUTPUT);
  
  _lastLoopTime = 0;
  _timlapseInterval = TIMELAPSE_INTERVAL_SECONDS * 1000L;
  
  // Turn the camera power on
  digitalWrite(CAM_POWER_PIN, HIGH);
  
}

void loop() {
  
  unsigned long curTime = millis();
  
  if (curTime - _lastLoopTime >= _timlapseInterval) 
  {
    
    Serial.println("Capturing image");
  
    // Turn the lights on
    digitalWrite(LED_POWER_PIN, HIGH);    
   
    delay(2000);
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Here we are replacing the HERO camera remote control via command scripts stored on the SD card with URL command sent over WiFi
    // for testing purposes
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // initialize the client library
    HttpClient client;
   
    // turn the camera on via WiFi in case turned off
    client.get("http://10.5.5.9/bacpac/PW?t=gopro3fish&p=%01");
    
    delay(2000);
    
    // set the resolution to 5mp
    client.get("http://10.5.5.9/camera/PR?t=gopro3fish&p=%03"); 
   
    delay(2000);

    // shutter the camera
    client.get("http://10.5.5.9/bacpac/SH?t=gopro3fish&p=%01");    
    
    delay(2000);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
        
    // Turn the lights off
    digitalWrite(LED_POWER_PIN, LOW);

    // get the camera status and print to serial port    
    // get camera name
    Serial.println("Getting camera type");
    client.get("http://10.5.5.9/camera/cv");
    Serial.print("Camera type: " );
    if (!client.exitValue()) {
      while (client.available()) {
        char c = client.read();
        Serial.print(c);
      }
      Serial.flush(); 
    }
    
    Serial.println("\nGetting camera status");
    byte statusByte[32];
    client.get("http://10.5.5.9/camera/se?t=gopro3fish");
    int i = 0;
    if (!client.exitValue()) {
      while (client.available()) {
        byte c = client.read();
        Serial.print(c);
        statusByte[i++] = c;
      }
      Serial.flush();
    }
    
    // photo count is at bytes 24-25
    // battery % is byte 20
    // more details here:
    // https://github.com/KonradIT/goprowifihack/blob/master/ByteStates.md  
    //char batteryStr = statusByte[19];
    byte photoCountHighByte = statusByte[23]; 
    byte photoCountLowByte = statusByte[24]; 
    int batteryPercent = (int)statusByte[19];
    int photoCount = (int) (photoCountLowByte + (photoCountHighByte << 8)); 
    Serial.print("\nphoto count: " ); Serial.print(photoCount); Serial.print("\t");
    Serial.print("battery percent: " ); Serial.print(batteryPercent); Serial.print("%\n"); 

    _lastLoopTime = millis();
    Serial.println("\Waiting for next sample time...");
  }  
}

