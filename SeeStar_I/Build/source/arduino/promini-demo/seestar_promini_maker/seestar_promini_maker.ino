// SeeStar GoPro Intervelometer
// Arduino ProMini AT328 3.3v 8MHz with SeeStar Shield and GoPro Hero 3+
// Author: Thom Maughan
// Date:   14 May 2015
// Version  1.0.9
// Copyright MBARI 2015
// Acknowledgments: All life forms are acknowledged
// Maker Faire Demo Code
// THIS CODE IS NOT INTENDED FOR BROAD DISTRIBUTION


#include <SPI.h>

// Digital IO symbolic definitions (All are Outputs except those with 'IN' comments)
#define ARD_LED        13    // same as SCK (conflicts with DS3234 SPI bus
#define RTC_INT_L      2     // IN  //WAKEUP_PIN = 2;
#define PIN3           3     // spare
#define PIN4           4     // active hi to turn on base of open collector flashlight button pusher
#define RTC_RESET_L    5     // active lo pulse on RTC to reset DS3224
#define UART_SHDN_L    6     // turn of the RS232 transciever (active lo)
#define LIGHT_POWER    7     // active hi to turn on power to DC/DC converter for LED flashlight
#define CAM_ON         8     // active hi to gate of npn transistor across pin 12 and pin 30 (gnd) of GoPro bus
#define CAM_POWER      9     // active hi to turn or power to GoPro battery eliminator
#define RTC_CS_L       10    // RTC chip select, active lo

// Analog INPUTs
#define BAT_VOLT    A0
#define BAT_CURR    A1

// variable declarations
int  ticCnt = 0;
int  minCnt = 0;

unsigned int batCurrent = 0;
unsigned int batVoltage = 0;

int sleepStatus = 0;                // variable to store a request for sleep
int count = 0;                      // counter
String inputString = "";            // a string to hold incoming data
boolean stringComplete = false;     // whether the string is complete

// millis() time variables
unsigned long TimerA;
long previousMillis = 0;            // variable to store last time LED was updated
long startTime ;                    // start time for stop watch
long elapsedTime ;                  // elapsed time for stop watch


void setup()
{
  // put your setup code here, it is run before the continuous loop function:

  pinMode(RTC_INT_L, INPUT);         //  p2
  pinMode(PIN4, OUTPUT);             //  p4 flashlight button
  pinMode(RTC_RESET_L, OUTPUT);      //  p5
  pinMode(UART_SHDN_L, OUTPUT);      //  p6
  pinMode(LIGHT_POWER, OUTPUT);      //  p7
  pinMode(CAM_ON, OUTPUT);           //  p8
  pinMode(CAM_POWER, OUTPUT);        //  p9
  pinMode(RTC_CS_L, OUTPUT);         //  p10
  //pinMode(ARD_LED, OUTPUT);        //  p13  (port 13 is also SPI bus SCK config'd in RTC_init()


  //digitalWrite(ARD_LED, LOW);      // LED off
  digitalWrite(RTC_RESET_L, HIGH);   // active lo uart reset
  digitalWrite(PIN4, LOW);           // gate of open collector connected to Flashlight button
  digitalWrite(UART_SHDN_L, LOW);    // active lo uart reset
  digitalWrite(LIGHT_POWER, LOW);    // active hi
  digitalWrite(CAM_ON, LOW);         // active hi, gate of open collector connected to pin12 of gopro bus (and pin 1 or 30 for gopro ground)
  digitalWrite(CAM_POWER, LOW);      // active hi
  digitalWrite(RTC_CS_L, HIGH);      // active lo

  Serial.begin(9600);

  RTC_init();   // Note: dont use Arduino LED (pin 13) and RTC

  // To Set RTC, uncomment the SetTimeDate line below with the correct time and date
  //day(1-31), month(1-12), year(0-99), hour(0-23), minute(0-59), second(0-59)
  //SetTimeDate(14,05,15,16,35,02); // 14 May 2015 4:35:02 pm

}

//do Not adjust MIN_INTERVAL (this is deterimined by how long it takes to power up and take an image and power down
#define MIN_INTERVAL  25000          // 25seconds

// adjust INTERVAL to determine intervelometer time between photos (30000 = 30 sec)
#define INTERVAL      30000          // msec between pictures, NO less than MIN_INTERVAL


void loop()
{
  // put your main code here, to run repeatedly:

  unsigned int msec;
  unsigned int duration;

  Serial.print(((float) (INTERVAL)) / 1000);
  Serial.println(" sec interval");

  Serial.println(ReadTimeDate());

  duration = powerup_take_image_powerdown();

  Serial.print(duration);
  Serial.println(" msec duration");

  Serial.println(ReadTimeDate());

  read_battery(1);    // 1 => print

  if (INTERVAL >= duration)
  {
    msec = INTERVAL - duration;
  }
  else
    msec = MIN_INTERVAL - duration; // minimum interval is 10 sec

  delay(msec);
  Serial.print(msec);
  Serial.println(" msec loop delay");
}


  //CAMPOWER: _|--------------------------------------------------------------------------------------------------------|__
  //CAM_ON:   ____||_______________________________________________________________________________________________________
  //LEDPOWER: ____|-----------------------------------------------------------------------|________________________________
  //LED_ON:   ______________________________________________________________||_____________________________________________
  //Take Pix: __________________________________________________________________________||_________________________________
  //Seconds:   0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21

  // on the root of the GoPro uSD is autoexec.ash, it should look like this (with Unix line terminations)
  //################################################
  //# Hero3+: Silver: wait 5, PhotoMode, wait 5, Take one image, wait 5 seconds (for image write?) and power down #
  //################################################
  //
  //sleep 5
  //t app appmode photo
  //sleep 5
  //t app button shutter PR
  //sleep 5
  //poweroff yes
  //reboot yes

// autoexec.ash delays (useed in the defines below)
#define ASH_BOOTDELAY      5000
#define ASH_MENUDELAY      5000   // delay after photomode and before shutter 
#define ASH_TAKEPHOTODELAY 5000

// definitions for the camera power and light power sequence / timeline
#define CAM_POWERUP_DELAY  1000   // was 1000
#define CAM_ON_BUTTON      100
#define LIGHT_ON_DELAY     2000   // delay after shutter to turn on Light
#define CAM_ON_TO_LIGHT_ON ASH_BOOTDELAY + ASH_MENUDELAY + LIGHT_ON_DELAY  // 12000  // ash script delays before PR are here: 5000ash + 5000ash + 2000
#define LIGHT_ON_BUTTON    200    // button press duration 100 does not work
#define LIGHT_ON_DURATION  3000   // 3000
#define CAM_OFF_DELAY      6000   


unsigned int powerup_take_image_powerdown()
{
  unsigned int i;
  unsigned int dur;
  unsigned int msec = 0;
  int prnBatFlg;
  unsigned long startTime;
  unsigned long deltaTime;

  prnBatFlg = 1;    // print battery readings, 1 says print

  startTime = millis();
  Serial.println(startTime);

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);

  read_battery(prnBatFlg);

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);

  // turn on the 16v input power to the CamDo battery eliminator in the GoPro
  Serial.println("Camera Power ON");
  digitalWrite(CAM_POWER, HIGH);

  // powerup delay for camera
  dur = CAM_POWERUP_DELAY;
  Serial.print(dur);
  Serial.println(" msec");
  delay(dur);
  msec += dur;

  read_battery(prnBatFlg);

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);

  // turn power on to the LED flashlight - this does not turn on the light, this enables the LED flashlight controller
  Serial.println("Light Power ON");
  digitalWrite(LIGHT_POWER, HIGH);
  
  // Push the GoPro camera 'on' button (pin 12 on GoPro bus)
  dur = CAM_ON_BUTTON;
  Serial.println("Take Picture");
  digitalWrite(CAM_ON, HIGH);
  delay(dur);          // small 'button push' delay (TODO: find out how small)
  digitalWrite(CAM_ON, LOW);
  msec += dur;

  // Delay between GoPro Camera ON button push and turning on LED flashlight (ash script delays + scene adjust alg)
  // LED flash light should come on with a couple seconds to allow GoPro auto white balance algorithm time to adjust
  dur = CAM_ON_TO_LIGHT_ON;
  Serial.print(dur);
  Serial.println(" msec");
  delay(dur);
  read_battery(prnBatFlg);
  msec += dur;

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);
  read_battery(prnBatFlg);

  // press the LED flashlight button
  dur = LIGHT_ON_BUTTON;
  Serial.println("Light Button Push");
  digitalWrite(PIN4, HIGH);
  delay(dur);
  digitalWrite(PIN4, LOW);
  msec += dur;

  read_battery(prnBatFlg);

  // light on duration, light should turn off just after GoPro takes picture (photo num increments in GoPro display)
  dur = LIGHT_ON_DURATION;
  Serial.print(dur);
  Serial.println(" msec");
  delay(dur);
  msec += dur;

  read_battery(prnBatFlg);

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);

  // Turn off the LED Flashlight
  Serial.println("Light Power OFF");
  digitalWrite(LIGHT_POWER, LOW);

  read_battery(prnBatFlg);

  // wait a period of time to insure image is written to uSD card
  dur = CAM_OFF_DELAY;
  Serial.print(dur);
  Serial.println(" msec");
  for (i = 0; i < 10; i++)
  {
    delay(dur / 10);
    msec += dur / 10;
    read_battery(prnBatFlg);
    if (batCurrent < 80)   // batCurrent is 60mA to 80mA typically after GoPro shuts itself down
    {
      // a little more delay just to be safe
      delay(dur / 10);
      msec += dur / 10;      
      Serial.println("GoPro powered off");
      //break;   // break and GoPro will be powered off based on current, break commmented for safe delay
    }
  }

  deltaTime = millis() - startTime;
  Serial.println(deltaTime);

  Serial.println("Camera Power OFF");
  digitalWrite(CAM_POWER, LOW);

  return (msec);
}


// same routine as above with debug statements removed
unsigned int simple_powerup_take_image_powerdown()
{
  unsigned int dur;
  unsigned int msec = 0;
  
  // turn on the 16v input power to the CamDo battery eliminator in the GoPro
  digitalWrite(CAM_POWER, HIGH);

  // powerup delay for GoPro / Battery Eliminator
  dur = CAM_POWERUP_DELAY;
  delay(dur);
  msec += dur;

  // turn power on to the LED flashlight
  digitalWrite(LIGHT_POWER, HIGH);
  
  // Push the GoPro camera 'on' button (pin 12 on GoPro bus)
  dur = CAM_ON_BUTTON;
  digitalWrite(CAM_ON, HIGH);
  delay(dur);          // 'button push' delay
  digitalWrite(CAM_ON, LOW);
  msec += dur;

  // Delay between GoPro Camera ON button push and turning on LED flashlight (ash script delays + scene adjust alg)
  dur = CAM_ON_TO_LIGHT_ON;
  delay(dur);
  msec += dur;

  // press the LED flashlight button
  dur = LIGHT_ON_BUTTON;
  digitalWrite(PIN4, HIGH);
  delay(dur);
  digitalWrite(PIN4, LOW);
  msec += dur;

  // light on duration, light should turn off just after GoPro takes picture (photo num increments in GoPro display)
  dur = LIGHT_ON_DURATION;
  delay(dur);
  msec += dur;

  // Turn off the LED Flashlight
  digitalWrite(LIGHT_POWER, LOW);

  // wait a period of time to insure image is written to uSD card
  dur = CAM_OFF_DELAY;
  delay(dur);

  digitalWrite(CAM_POWER, LOW);

  return (msec);
}




int read_battery(int prnFlg)
{
  unsigned long ulBat;
  unsigned int voltCnt, currCnt;
  float batVolt;

  voltCnt = (unsigned int) analogRead(A0);
  currCnt = (unsigned int) analogRead(A1);

  ulBat = (unsigned long) voltCnt;
  ulBat *= 3184;              // 504 cnts = 16.05v implies 31.85 scale factor
  ulBat /= 100;
  batVoltage = (unsigned int) ulBat;

  batCurrent = currCnt * 49;    // scale to milliamps (approx) (4.88 mA / cnt)
  batCurrent /= 10;            // 4.9

  if (prnFlg == 1)
  {
    Serial.print("Voltage = ");
    batVolt = (float)batVoltage;
    batVolt /= 1000;
    Serial.print(batVolt);
    Serial.print(" V ");
    //Serial.print(voltCnt);    // uncomment to check calibration
    //Serial.print(" cnt ");
    
    Serial.print("  Current = ");
    Serial.print(batCurrent);
    Serial.print(" mA ");
    //Serial.print(currCnt);
    //Serial.print(" cnt");
    Serial.println(" ");
  }

}


int RTC_init()
{
  pinMode(RTC_CS_L, OUTPUT); // chip select

  // start the SPI library:
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE1); // both mode 1 & 3 should work

  //set control register
  digitalWrite(RTC_CS_L, LOW);
  SPI.transfer(0x8E);
  SPI.transfer(0x60); //60= disable Osciallator and Battery SQ wave @1hz, temp compensation, Alarms disabled
  digitalWrite(RTC_CS_L, HIGH);
  delay(10);
}
//=====================================
int SetTimeDate(int d, int mo, int y, int h, int mi, int s)
{
  int TimeDate [7] = {s, mi, h, 0, d, mo, y};
  for (int i = 0; i <= 6; i++)
  {
    if (i == 3)
      i++;
    int b = TimeDate[i] / 10;
    int a = TimeDate[i] - b * 10;
    if (i == 2)
    {
      if (b == 2)
        b = B00000010;
      else if (b == 1)
        b = B00000001;
    }
    TimeDate[i] = a + (b << 4);

    digitalWrite(RTC_CS_L, LOW);
    SPI.transfer(i + 0x80);
    SPI.transfer(TimeDate[i]);
    digitalWrite(RTC_CS_L, HIGH);
  }
}
//=====================================
String ReadTimeDate()
{
  String temp;
  int TimeDate [7]; //second,minute,hour,null,day,month,year

  for (int i = 0; i <= 6; i++)
  {
    if (i == 3)
      i++;
    digitalWrite(RTC_CS_L, LOW);
    SPI.transfer(i + 0x00);
    unsigned int n = SPI.transfer(0x00);
    digitalWrite(RTC_CS_L, HIGH);
    int a = n & B00001111;
    if (i == 2)
    {
      int b = (n & B00110000) >> 4; //24 hour mode
      if (b == B00000010)
        b = 20;
      else if (b == B00000001)
        b = 10;
      TimeDate[i] = a + b;
    }
    else if (i == 4)
    {
      int b = (n & B00110000) >> 4;
      TimeDate[i] = a + b * 10;
    }
    else if (i == 5)
    {
      int b = (n & B00010000) >> 4;
      TimeDate[i] = a + b * 10;
    }
    else if (i == 6)
    {
      int b = (n & B11110000) >> 4;
      TimeDate[i] = a + b * 10;
    }
    else
    {
      int b = (n & B01110000) >> 4;
      TimeDate[i] = a + b * 10;
    }
  }
  temp.concat(TimeDate[4]);
  temp.concat("/") ;
  temp.concat(TimeDate[5]);
  temp.concat("/") ;
  temp.concat(TimeDate[6]);
  temp.concat("     ") ;
  temp.concat(TimeDate[2]);
  temp.concat(":") ;
  temp.concat(TimeDate[1]);
  temp.concat(":") ;
  temp.concat(TimeDate[0]);
  return (temp);
}

// RTC Test Code - uncomment and call from while(1) in loop
//void rtc_read_loop()
//{
//  Serial.println(ReadTimeDate());
//  delay(1000);
//}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent()
{
  while (Serial.available())
  {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n')
    {
      stringComplete = true;
    }

    stringComplete = true;    // debug
  }
}




