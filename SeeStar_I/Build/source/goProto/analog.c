/****************************************************************************/
/* Created 2010 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include <p24fxxxx.h>
#include "analog.h"

#include "sys_defs.h"

#define ADC_DEBUGGING   0

void anaInit()
{
    AD1CON1 = 0x00E0;       /* SSRC<2:0> = 111 implies internal counter ends 
                            sampling and starts converting. */
    AD1CON2 = 0;
    AD1CON3 = 0x0F02;       /* Sample time = 15Tad, Tad = 3Tcy */

    AD1CSSL = 0;            /* Disable input scanning */
    AD1CON1bits.ADON = 1;   /* turn ADC ON */
}

unsigned int anaGetSample(int channel)
{
    /* Connect the analog pin to the S/H input */    
    switch ( channel )
    {  
        case ANA_CURRENT: AD1CHS = ANA_CURRENT; break;
        case ANA_VOLTAGE: AD1CHS = ANA_VOLTAGE; break;
        default: return 0;
    }

    /* start sampling, after Tad go to conversion */
    AD1CON1bits.SAMP = 1;

#if ADC_DEBUGGING
_LATB4 = 1;
#endif

    /* wait for sample to complete */
    while ( AD1CON1bits.SAMP )
        ; /* sample complete? */

#if ADC_DEBUGGING
_LATB4 = 0;
Nop();
_LATB4 = 1;
#endif

    /* wait for conversion to complete */
    while ( !AD1CON1bits.DONE )
        ; /* conversion done */

#if ADC_DEBUGGING
_LATB4 = 0;
#endif
    
    /* return the current ADC value */
    return ADC1BUF0;
}

unsigned int anaMilliAmps()
{
    double cnts_f;
    unsigned int milliamps;
    
    /* sample the bus current */
    cnts_f = (double)anaGetSample(ANA_CURRENT);
    
    /* apply slope and offest */
    cnts_f *= M_CURR_CAL;
    cnts_f += B_CURR_CAL;

    /* convert to millivolts*/
    cnts_f *= 1000.0;

    milliamps = (unsigned int)cnts_f;

    return milliamps;

}

unsigned int anaMilliVolts()
{
    double cnts_f;
    unsigned int millivolts;
    
    /* sample the bus voltage */
    cnts_f = (double)anaGetSample(ANA_VOLTAGE);
    
    /* apply slope and offest */
    cnts_f *= M_VOLT_CAL;
    cnts_f += B_VOLT_CAL;

    /* convert to millivolts*/
    cnts_f *= 1000.0;

    millivolts = (unsigned int)cnts_f;

    return millivolts;
}

