/****************************************************************************/
/* Created 2006 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef PARSER_H
#define PARSER_H

#include "commands.h"

int prsNameMatch(char* s, char* ref);
char* prsSkip(char* s, char* ignore);
int prsIsInteger(char* txt);
int prsIsValidNameChar(char c);
int prsParse(char* line, const CmdStruct* cmds, int err_code);

#endif
