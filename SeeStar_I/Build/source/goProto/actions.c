/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/
#include <p24fxxxx.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "analog.h"
#include "actions.h"
#include "errors.h"
#include "parser.h"
#include "serial.h"
#include "config.h"
#include "spi.h"
#include "i2c.h"
#include "ex_rtc.h"
#include "data_flash.h"
#include "hero_io.h"
#include "tasks.h"
#include "sys_timer.h"

#include "sys_defs.h"

static char msg_buff[64];
static ConfigData configData;

static void actPutOnOff(int arg)
{
    if ( arg )
        serPutString(SER_CONSOLE, "ON");
    else
        serPutString(SER_CONSOLE, "OFF");

    return;
}

static void actPutTrueFalse(int arg)
{
    if ( arg )
        serPutString(SER_CONSOLE, "TRUE");
    else
        serPutString(SER_CONSOLE, "FALSE");

    return;
}

/****************************************************************************/
/*                               Root commands                              */
/****************************************************************************/
int actGetCmd(char* s)
{
    return prsParse(s, cmdsGet, ERR_NO_SUB_COMMAND);
}

int actSetCmd(char* s)
{
    return prsParse(s, cmdsSet, ERR_NO_SUB_COMMAND);
}

int actClrCmd(char* s)
{
    return prsParse(s, cmdsClr, ERR_NO_SUB_COMMAND);
}

int actEEProgCmd(char* s)
{
    unsigned char i;
    unsigned char address;

    for (address = 0; address < 128; address++)
    {
        i2cStart();
        i2cSendByte(0xA0);            
        i2cSendByte(address);
        
        if ( address == 0 ) 
            i2cSendByte (0x05);
        else 
            i2cSendByte (0xFF);
        
        i2cStop ();
        
        serPutString(SER_CONSOLE, ".");

        for (i = 0; i < 100; i++)
            i2cDelay(255);
    }
    
    serPutString(SER_CONSOLE, "\r\n");
    
    return ERR_SUCCESS;
}

int actEEReadCmd(char* s)
{
    unsigned char address;
    unsigned int data;

    for (address = 0; address < 128; address++)
    {
        i2cStart();
        /* control code = 1010, block address = XXX, R/W_n = 0 */
        i2cSendByte(0xA0);            
        i2cSendByte(address);
        i2cStart();
        /* control code = 1010, block address = XXX, R/W_n = 1 */
        i2cSendByte(0xA1);            
        data = i2cReadByte();
        i2cStop();

        sprintf(msg_buff, "%02x", data);
        serPutString(SER_CONSOLE, msg_buff);
        serPutString(SER_CONSOLE, " ");
        if ( (address & 0xf) == 0xf )
            serPutString(SER_CONSOLE, "\r\n");
    }
    
    return ERR_SUCCESS;
}

int actCamOnCmd(char* s)
{
    hroCamOn();

    return ERR_SUCCESS;
}

int actCamOffCmd(char* s)
{
    hroCamOff();

    return ERR_SUCCESS;
}

int actSnapHiCmd(char* s)
{
    hroSnapHi();
    
    return ERR_SUCCESS;
}

int actSnapLoCmd(char* s)
{
    hroSnapLo();

    return ERR_SUCCESS;
}

int actVidOnCmd(char* s)
{
    hroStartVid();
    
    return ERR_SUCCESS;
}

int actVidOffCmd(char* s)
{
    hroStopVid();
    
    return ERR_SUCCESS;
}

int actLedOnCmd(char* s)
{
    serPutString(SER_CONSOLE, "LED ON (LATC7 = 1)\r\n");
    _LATC7 = 1;

    return ERR_SUCCESS;
}

int actLedOffCmd(char* s)
{
    serPutString(SER_CONSOLE, "LED OFF (LATC7 = 0)\r\n");
    _LATC7 = 0;

    return ERR_SUCCESS;
}

int actPwrOnCmd(char* s)
{
    hroPwrOn();
    return ERR_SUCCESS;
}

int actPwrOffCmd(char* s)
{
    hroPwrOff();
    return ERR_SUCCESS;
}

int actTimeCmd(char* s)
{
    rtcTimeDateStr(msg_buff);
    serPutString(SER_CONSOLE, msg_buff);

    serPutString(SER_CONSOLE, "\r\n");
    
    return ERR_SUCCESS;
}

int actHelpCmd(char* s)
{
    serPutString(SER_CONSOLE, "PARAM - display current user parameters\r\n");
    serPutString(SER_CONSOLE, "DEFAULT - set user parameters to default\r\n");
    serPutString(SER_CONSOLE, "SAVE - save user parameters to flash\r\n");
    serPutString(SER_CONSOLE, "TIME - display the RTC time\r\n");
    serPutString(SER_CONSOLE, "HELP - display this help menu\r\n");
    serPutString(SER_CONSOLE, "RESET - reset the PIC microcontroller\r\n");
    serPutString(SER_CONSOLE, "LED ON - toggle the LED relay on\r\n");
    serPutString(SER_CONSOLE, "LED OFF - toggle the LED relay off\r\n");
    serPutString(SER_CONSOLE, "SAMPLE NOW - take a sample now\r\n");

    serPutString(SER_CONSOLE, "GET LED - get the LED relay state\r\n");
    serPutString(SER_CONSOLE, "GET POWER - get the power consumption\r\n");

    serPutString(SER_CONSOLE, "SET TIME - set the RTC time\r\n");
    serPutString(SER_CONSOLE, "SET AUTO RUN MODE - set the auto run mode [ON|OFF]\r\n");
    serPutString(SER_CONSOLE, "SET AUTO RUN DELAY - set the auto run delay (SEC)\r\n");
    serPutString(SER_CONSOLE, "SET AUTO RUN DURATION - set the auto run duration (SEC)\r\n");
    serPutString(SER_CONSOLE, "SET SAMPLE MODE - set the sample mode [OFF|PIC|VID|HR3]\r\n");
    serPutString(SER_CONSOLE, "SET SAMPLE INTERVAL - set the interval of the sample (SEC)\r\n");
    serPutString(SER_CONSOLE, "SET SAMPLE DURATION - set the duration of the sample (SEC)\r\n");
    serPutString(SER_CONSOLE, "SET LED ON DELAY - set the LED relay delay (HR3 only)\r\n");
    serPutString(SER_CONSOLE, "SET LED ON DURATION - set the LED relay duration (HR3 only)\r\n");
/*    serPutString(SER_CONSOLE, "SET VOLT STOP LEVEL - set the voltage level to stop at \r\n");*/

    if ( prsNameMatch(s, "ALL") )
    {
        serPutString(SER_CONSOLE, "\r\n******* LOW LEVEL COMMANDS *******\r\n");
        serPutString(SER_CONSOLE, "EEPROG - program the on board EEPROM\r\n");
        serPutString(SER_CONSOLE, "EEREAD - read the on board EEPROM\r\n");
        serPutString(SER_CONSOLE, "CAM ON - turn on the GoPro via GP_MODE\r\n");
        serPutString(SER_CONSOLE, "CAM OFF - turn off the GoPro via GP_MODE\r\n");
        serPutString(SER_CONSOLE, "SNAPHI - excecute the snap hi I/O sequence\r\n");
        serPutString(SER_CONSOLE, "SNAPLO - execute the snap lo I/O sequence\r\n");
        serPutString(SER_CONSOLE, "VID ON - set GP_ID2 lo to start video recording\r\n");
        serPutString(SER_CONSOLE, "VID OFF - set GP_ID2 hi to stop video recording\r\n"); 
        serPutString(SER_CONSOLE, "PWR ON - apply power to the GoPro\r\n");
        serPutString(SER_CONSOLE, "PWR OFF - remove power from the GoPro\r\n");

        serPutString(SER_CONSOLE, "GET DEBUG - display which modules have debugging enabled\r\n");
        serPutString(SER_CONSOLE, "SET DEBUG - enable debugging for a module\r\n");
        serPutString(SER_CONSOLE, "CLEAR DEBUG - disable module debugging\r\n");

        serPutString(SER_CONSOLE, "******* LOW LEVEL COMMANDS *******\r\n");
    }

    return ERR_SUCCESS;
}

int actResetCmd(char* s)
{
    asm("reset");
    return ERR_SUCCESS;
}

int actSaveCmd(char* s)
{
    /* set the boot flag */
    configData.bootFlag = CFG_DEFAULT_BOOT_FLAG;
     
    /* get the current param settings and store them in the config struct */
    configData.autoRunMode = tskGetAutoRunMode();
    configData.autoRunDelay = tskGetAutoRunDelay();
    configData.autoRunDuration = tskGetAutoRunDuration();
    configData.sampleMode = tskGetSampleMode();
    configData.sampleInterval = tskGetSampleInterval();
    configData.sampleDuration = tskGetSampleDuration();
    configData.ledOnDelay = tskGetLedOnDelay();
    configData.ledOnDuration = tskGetLedOnDuration();
    configData.voltStopLevel = tskGetVoltStopLevel();

    /* set debug bit vector */
    configData.debugBits = CFG_DEBUG_BITS_DEF;

    if ( hroDebugState() )
        configData.debugBits += HRO_DEBUG_BIT;

    if ( tskDebugState() )
        configData.debugBits += TSK_DEBUG_BIT;

    /* write the config to flash memory */
    if ( cfgWrite(&configData) < 0 )
    {
        serPutString(SER_CONSOLE, "ERR: CONFIG WRITE\r\n");
        return ERR_FAILURE;
    }

    return ERR_SUCCESS;
}

#define DEFAULT     "[DFLT]"
#define CHANGED     "      "

int actParamCmd(char* s)
{
    unsigned int mode;
    unsigned long val;

    char* default_str;
    char* mode_str;

    /* show autoRun state */
    mode = tskGetAutoRunMode();

    if ( mode == CFG_AUTO_RUN_MODE_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    if ( mode == ON )
        mode_str = "ON";
    else if ( mode == OFF )
        mode_str = "OFF";
    else
        mode_str = "???";

    sprintf(msg_buff, "AUTO RUN MODE     %s : %s\r\n", default_str, mode_str);
    serPutString(SER_CONSOLE, msg_buff);

    /* show autoRunDelay state */
    val = tskGetAutoRunDelay(); 

    if ( val == CFG_AUTO_RUN_DELAY_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "AUTO RUN DELAY    %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show autoRunDuration state */
    val = tskGetAutoRunDuration(); 

    if ( val == CFG_AUTO_RUN_DURATION_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "AUTO RUN DURATION %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show sampleMode state */
    mode = tskGetSampleMode();

    if ( mode == CFG_SAMPLE_MODE_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    if ( mode == CFG_SAMPLE_MODE_OFF )
        mode_str = "OFF";
    else if ( mode == CFG_SAMPLE_MODE_PIC )
        mode_str = "PIC";
    else if ( mode == CFG_SAMPLE_MODE_VID )
        mode_str = "VID";
    else if ( mode == CFG_SAMPLE_MODE_HR3 )
        mode_str = "HERO3";
    else
        mode_str = "???";

    sprintf(msg_buff, "SAMPLE MODE       %s : %s\r\n", default_str, mode_str);
    serPutString(SER_CONSOLE, msg_buff);

    /* show sampleInterval state */
    val = tskGetSampleInterval(); 

    if ( val == CFG_SAMPLE_INTERVAL_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "SAMPLE INTERVAL   %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show sampleDuration state */
    val = tskGetSampleDuration(); 

    if ( val == CFG_SAMPLE_DURATION_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "SAMPLE DURATION   %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show ledOnDelay state */
    val = tskGetLedOnDelay(); 

    if ( val == CFG_LED_ON_DELAY_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "LED ON DELAY      %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show ledOnDuration state */
    val = tskGetLedOnDuration(); 

    if ( val == CFG_LED_ON_DURATION_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "LED ON DURATION   %s : %lu SEC\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);

    /* show voltStopLevel state */
/*
    val = tskGetVoltStopLevel(); 

    if ( val == CFG_VOLT_STOP_LEVEL_DEF )
        default_str = DEFAULT;
    else
        default_str = CHANGED;

    sprintf(msg_buff, "VOLT STOP LEVEL   %s : %lu mV\r\n", default_str, val);
    serPutString(SER_CONSOLE, msg_buff);
*/ 

    return ERR_SUCCESS;
}

int actDefaultCmd(char* s)
{
    /* set all config values to default, but don't save to flash */
    tskSetAutoRunMode(CFG_AUTO_RUN_MODE_DEF);
    tskSetAutoRunDelay(CFG_AUTO_RUN_DELAY_DEF);
    tskSetAutoRunDuration(CFG_AUTO_RUN_DURATION_DEF);
    tskSetSampleMode(CFG_SAMPLE_MODE_DEF);
    tskSetSampleInterval(CFG_SAMPLE_INTERVAL_DEF);
    tskSetSampleDuration(CFG_SAMPLE_DURATION_DEF);
    tskSetLedOnDelay(CFG_LED_ON_DELAY_DEF);
    tskSetLedOnDuration(CFG_LED_ON_DURATION_DEF);
    tskSetVoltStopLevel(CFG_VOLT_STOP_LEVEL_DEF);

    /* debugging is off for all modules by default */
    tskDebugOff();
    hroDebugOff();

    return ERR_SUCCESS;
}

int actSampleNowCmd(char* s)
{
    tskSampleNow();
    return ERR_SUCCESS;
}

int actUptimeCmd(char* s)
{

    unsigned long secs = (tmrGetTicks() / TMR_FREQ);

    sprintf(msg_buff, "UPTIME %lu SECONDS\r\n", secs);
    serPutString(SER_CONSOLE, msg_buff);

    return ERR_SUCCESS;
}

int actMemEraseCmd(char* s)
{
    dmErase();
    
    return ERR_SUCCESS;
}

int actMemWriteCmd(char* s)
{
    dmPutString(s);    
    dmPutString("\r\n");    
    
    return ERR_SUCCESS;
}

#define READ_CHUNK  16
int actMemReadCmd(char* s)
{
    int i;
    int read_complete = FALSE;
    unsigned long read_ptr = 0;

    while (!read_complete)
    {
        /* read a chunk of bytes */
        dmRead(read_ptr, msg_buff, READ_CHUNK);
        read_ptr += READ_CHUNK; 
        
        i = 0;
        while ( i < READ_CHUNK )
        {
            if ( msg_buff[i] & 0x80 )
            {
                read_complete = TRUE;
                i = READ_CHUNK;
            }
            else
            {
                if ( serPutByte(SER_CONSOLE, (unsigned char)msg_buff[i]) )
                    ++i;
            }
        }
    }

    serPutString(SER_CONSOLE, "\r\n");
        
    return ERR_SUCCESS;
}

/* misc test functions */
int actTest1Cmd(char* s)
{
    serPutString(SER_CONSOLE, "stub for actTest1Cmd(char* s)\r\n");
    
    return ERR_SUCCESS;
}

int actTest2Cmd(char* s)
{
    serPutString(SER_CONSOLE, "stub for actTest2Cmd(char* s)\r\n");

    return ERR_SUCCESS;
}

int actTest3Cmd(char* s)
{
    serPutString(SER_CONSOLE, "stub for actTest3Cmd(char* s)\r\n");

    return ERR_SUCCESS;
}

int actTest4Cmd(char* s)
{
    serPutString(SER_CONSOLE, "stub for actTest4Cmd(char* s)\r\n");

    return ERR_SUCCESS;
}

/****************************************************************************/
/*                               Get commands                               */
/****************************************************************************/
int actGetLedCmd(char* s)
{
    serPutString(SER_CONSOLE, "LED IS ");
    actPutOnOff(_RC8);
    serPutString(SER_CONSOLE, "\r\n");
    
    return ERR_SUCCESS;
}

int actGetDebugCmd(char* s)
{
    serPutString(SER_CONSOLE, "hroDebugState() : ");
    actPutTrueFalse(hroDebugState());
    serPutString(SER_CONSOLE, "\r\n");

    serPutString(SER_CONSOLE, "tskDebugState() : ");
    actPutTrueFalse(tskDebugState());
    serPutString(SER_CONSOLE, "\r\n");

    return ERR_SUCCESS;
}

int actGetPowerCmd(char* s)
{
    double volts;
    double amps;
    double watts;
    
    /* read analog values */
    amps = (double)anaGetSample(4);
    volts = (double)anaGetSample(5);

    /* apply current slope and offest */
    amps *= M_CURR_CAL;
    amps += B_CURR_CAL;
    
    /* apply voltage slope and offest */
    volts *= M_VOLT_CAL;
    volts += B_VOLT_CAL;

    /* calculate power */
    watts = volts * amps;

    sprintf(msg_buff, "BUS VOLTAGE : %4.2f VOLTS\r\n", volts);
    serPutString(SER_CONSOLE, msg_buff);
    sprintf(msg_buff, "BUS CURRENT : %4.3f AMPS\r\n", amps);
    serPutString(SER_CONSOLE, msg_buff);
    sprintf(msg_buff, "BUS POWER   : %5.3f WATTS\r\n", watts);
    serPutString(SER_CONSOLE, msg_buff);
    
    return ERR_SUCCESS;
}

/****************************************************************************/
/*                               Set commands                               */
/****************************************************************************/
int actSetTimeCmd(char* s)
{
    int n;
    RtcTime rtc;

    rtc.seconds = 0;
    rtc.minutes = 0;
    rtc.hours = 0;
    rtc.day = 0;
    rtc.date = 0;
    rtc.month = 0;
    rtc.year = 0;

    n = sscanf(s, "%d/%d/%d %d:%d:%d %d", &rtc.month, &rtc.date, &rtc.year, 
               &rtc.hours, &rtc.minutes, &rtc.seconds, &rtc.day);

    if ( n < 7 )
    {
        serPutString(SER_CONSOLE, "ERR: BAD FORMAT\r\n");
        serPutString(SER_CONSOLE, "USAGE: MM/DD/YY HH:MM:SS DAY (");
        serPutString(SER_CONSOLE, "DAY = 1->Sun, 2->Mon, ... , 7->Sat)\r\n");
        return ERR_SUCCESS;
    }

    /* range check month */
    if ( (rtc.month < 1) || (rtc.month > 12) )
    {
        serPutString(SER_CONSOLE, "ERR: MONTH OUT OF RANGE (1 - 12)\r\n");
        return ERR_SUCCESS;
    }

    /* range check date */
    if ( (rtc.date < 1) || (rtc.date > 31) )
    {
        serPutString(SER_CONSOLE, "ERR: DAY OUT OF RANGE (1 - 31)\r\n");
        return ERR_SUCCESS;
    }

    /* range check year */
    if ( (rtc.year < 11) || (rtc.year > 99) )
    {
        serPutString(SER_CONSOLE, "ERR: YEAR OUT OF RANGE  (11 - 99)\r\n");
        return ERR_SUCCESS;
    }

    /* range check hour */
    if ( (rtc.hours < 0) || (rtc.hours > 23) )
    {
        serPutString(SER_CONSOLE, "ERR: HOURS OUT OF RANGE  (1 - 24)\r\n");
        return ERR_SUCCESS;
    }
    
    /* range check minutes */
    if ( (rtc.minutes < 0) || (rtc.minutes > 59) )
    {
        serPutString(SER_CONSOLE, "ERR: MINUTES OUT OF RANGE  (0 - 59)\r\n");
        return ERR_SUCCESS;
    }

    /* range check seconds */
    if ( (rtc.seconds < 0) || (rtc.seconds > 59) )
    {
        serPutString(SER_CONSOLE, "ERR: SECONDS OUT OF RANGE  (0 - 59)\r\n");
        return ERR_SUCCESS;
    }

    /* range check day */
    if ( (rtc.day < 1) || (rtc.day > 7) )
    {
        serPutString(SER_CONSOLE, "ERR: DAY OUT OF RANGE  (1 - 7)\r\n");
        return ERR_SUCCESS;
    }

    /* set the time */
    rtcSet(&rtc);

    /* show the user what you set the time to */
    rtcTimeDateStr(msg_buff);
    serPutString(SER_CONSOLE, "TIME SET TO: ");
    serPutString(SER_CONSOLE, msg_buff);
    serPutString(SER_CONSOLE, "\r\n");
    
    return ERR_SUCCESS;
}

int actSetDebugCmd(char* s)
{
    if ( prsNameMatch(s, "ALL") )
    {
        hroDebugOn();
        tskDebugOn();
        return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "HRO") )
    {
        hroDebugOn();
        return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "TSK") )
    {
        tskDebugOn();
        return ERR_SUCCESS;
    }

    serPutString(SER_CONSOLE, "ERR: SET DEBUG [ALL|HRO|TSK]\r\n");
    return ERR_SUCCESS;
}

int actSetAutoRunModeCmd(char* s)
{    
    if ( prsNameMatch(s, "ON") )
    {
         tskSetAutoRunMode(ON);
         return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "OFF") )
    {
         tskSetAutoRunMode(OFF);
         return ERR_SUCCESS;
    }

    serPutString(SER_CONSOLE, "ERR: SET AUTO RUN MODE [ON|OFF]\r\n");
    return ERR_SUCCESS;
}

int actSetAutoRunDelayCmd(char* s)
{
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetAutoRunDelay((unsigned long)val);
    
    return ERR_SUCCESS;
}

int actSetAutoRunDurationCmd(char* s)
{    
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetAutoRunDuration((unsigned long)val);

    return ERR_SUCCESS;
}

int actSetSampleModeCmd(char* s)
{    
    if ( prsNameMatch(s, "OFF") )
    {
         tskSetSampleMode(CFG_SAMPLE_MODE_OFF);
         return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "PIC") )
    {
         tskSetSampleMode(CFG_SAMPLE_MODE_PIC);
         return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "VID") )
    {
         tskSetSampleMode(CFG_SAMPLE_MODE_VID);
         return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "HR3") )
    {
         tskSetSampleMode(CFG_SAMPLE_MODE_HR3);
         return ERR_SUCCESS;
    }

    serPutString(SER_CONSOLE, "ERR: SET SAMPLE MODE [OFF|PIC|VID|HR3]\r\n");
    return ERR_SUCCESS;
}

int actSetSampleIntervalCmd(char* s)
{    
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetSampleInterval((unsigned long)val);

    return ERR_SUCCESS;
}

int actSetSampleDurationCmd(char* s)
{    
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetSampleDuration((unsigned long)val);

    return ERR_SUCCESS;
}

int actSetLedOnDelayCmd(char* s)
{    
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetLedOnDelay((unsigned long)val);

    return ERR_SUCCESS;
}

int actSetLedOffDelayCmd(char* s)
{    
    long val = atol(s);

    /* don't allow negative times*/
    if ( val < 0 )
        val = 0; 

    tskSetLedOnDuration((unsigned long)val);

    return ERR_SUCCESS;
}

int actSetVoltStopLevelCmd(char* s)
{    

    long val = atol(s);

    /* don't allow negative voltages */
    if ( val < 0 )
        val = 0; 

    tskSetVoltStopLevel((unsigned long)val);

    return ERR_SUCCESS;
}

/****************************************************************************/
/*                              Clear commands                              */
/****************************************************************************/
int actClrDebugCmd(char* s)
{
    if ( prsNameMatch(s, "ALL") )
    {
        hroDebugOff();
        tskDebugOff();
        return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "HRO") )
    {
        hroDebugOff();
        return ERR_SUCCESS;
    }

    if ( prsNameMatch(s, "TSK") )
    {
        tskDebugOff();
        return ERR_SUCCESS;
    }

    serPutString(SER_CONSOLE, "ERR: CLEAR DEBUG [ALL|HRO|TSK]\r\n");
    return ERR_SUCCESS;
}

