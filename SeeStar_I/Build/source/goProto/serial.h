/****************************************************************************/
/* Created 2009 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef SERIAL_H
#define SERIAL_H

void serInit(); 

int serGetByte(int port, unsigned char* b);
int serPutByte(int port, unsigned char b);
int serPutString(int port, char* buff);

int serIsRxFull(int port);
int serIsTxFull(int port);

int serGetRxOverFlow(int port);
void serClrRxOverFlow(int port);

void serRxFlush(int port);
void serTxFlush(int port);

#endif

