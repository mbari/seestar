/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include "config.h"
#include "flash_mem.h"
#include "sys_defs.h"

#include <string.h>

typedef struct
{
    ConfigData config;
    unsigned int checksum;
} ConfigBlock;

unsigned int calcCheckSum(char* buff, int size);


/****************************************************************************/
/*                             Config functions                             */
/****************************************************************************/
int cfgCheck(ConfigData* config)
{

    if ( config->bootFlag != CFG_DEFAULT_BOOT_FLAG )
        return -2;
    
    if ( (config->autoRunMode != ON) && (config->autoRunMode != OFF) )
        return -3;

    if ( (config->autoRunDelay < CFG_AUTO_RUN_DELAY_MIN) || 
         (config->autoRunDelay > CFG_AUTO_RUN_DELAY_MAX) )
        return -4;

    if ( (config->autoRunDuration < CFG_AUTO_RUN_DURATION_MIN) || 
         (config->autoRunDuration > CFG_AUTO_RUN_DURATION_MAX) )
        return -5;

    switch ( config->sampleMode ) 
    {
        case CFG_SAMPLE_MODE_OFF: break;
        case CFG_SAMPLE_MODE_PIC: break;
        case CFG_SAMPLE_MODE_VID: break;
        case CFG_SAMPLE_MODE_HR3: break;
        default: return -6;
    }

    if ( (config->sampleInterval < CFG_SAMPLE_INTERVAL_MIN) || 
         (config->sampleInterval > CFG_SAMPLE_INTERVAL_MAX) )
        return -7;

    if ( (config->sampleDuration < CFG_SAMPLE_DURATION_MIN) || 
         (config->sampleDuration > CFG_SAMPLE_DURATION_MAX) )
        return -8;

    if ( (config->ledOnDelay < CFG_LED_ON_DELAY_MIN) || 
         (config->ledOnDelay > CFG_LED_ON_DELAY_MAX) )
        return -9;

    if ( (config->ledOnDuration < CFG_LED_ON_DURATION_MIN) || 
         (config->ledOnDuration > CFG_LED_ON_DURATION_MAX) )
        return -10;

    if ( (config->voltStopLevel < CFG_VOLT_STOP_LEVEL_MIN) || 
         (config->voltStopLevel > CFG_VOLT_STOP_LEVEL_MAX) )
        return -11;

    return 0;
}

int cfgRead(ConfigData* config)
{
    ConfigBlock cb;
    unsigned int cs_calc;

    /* read the configuation struct from memory */
    memRead((char*)&cb, sizeof(cb));

    cs_calc = calcCheckSum((char*)&cb.config, sizeof(ConfigData));

    if ( cs_calc != cb.checksum )
        return -1;

    /* assign stored data to the config data struct */
    config->bootFlag = cb.config.bootFlag;
    config->autoRunMode = cb.config.autoRunMode;
    config->autoRunDelay = cb.config.autoRunDelay;
    config->autoRunDuration = cb.config.autoRunDuration;
    config->sampleMode = cb.config.sampleMode;
    config->sampleInterval = cb.config.sampleInterval;
    config->sampleDuration = cb.config.sampleDuration;
    config->ledOnDelay = cb.config.ledOnDelay;
    config->ledOnDuration = cb.config.ledOnDuration;
    config->voltStopLevel = cb.config.voltStopLevel;
    config->debugBits = cb.config.debugBits;

    return cfgCheck(config);
}

int cfgWrite(ConfigData* config)
{
    ConfigBlock cb;
    
    /* if config params are not in a valid range, don't write it */
    if ( cfgCheck(config) != 0 )
        return -1;

    /* assign stored data to the config data struct */
    cb.config.bootFlag = config->bootFlag;
    cb.config.autoRunMode = config->autoRunMode;
    cb.config.autoRunDelay = config->autoRunDelay;
    cb.config.autoRunDuration = config->autoRunDuration;
    cb.config.sampleMode = config->sampleMode;
    cb.config.sampleInterval = config->sampleInterval;
    cb.config.sampleDuration = config->sampleDuration;
    cb.config.ledOnDelay = config->ledOnDelay;
    cb.config.ledOnDuration = config->ledOnDuration;
    cb.config.voltStopLevel = config->voltStopLevel;
    cb.config.debugBits = config->debugBits;

    /* calc and assign the checksum */
    cb.checksum = calcCheckSum((char*)&cb.config, sizeof(ConfigData));

    /* erase the old config */
    memErase();

    /* write the new config */
    memWrite((char*)&cb, sizeof(cb));
    
    return 0;
}

int cfgDefault()
{
    ConfigBlock cb;

    /* assign stored data to the config data struct */
    cb.config.bootFlag = CFG_DEFAULT_BOOT_FLAG; 
    cb.config.autoRunMode = CFG_AUTO_RUN_MODE_DEF;
    cb.config.autoRunDelay = CFG_AUTO_RUN_DELAY_DEF;
    cb.config.autoRunDuration = CFG_AUTO_RUN_DURATION_DEF;
    cb.config.sampleMode = CFG_SAMPLE_MODE_DEF;
    cb.config.sampleInterval = CFG_SAMPLE_INTERVAL_DEF;
    cb.config.sampleDuration = CFG_SAMPLE_DURATION_DEF;
    cb.config.ledOnDelay = CFG_LED_ON_DELAY_DEF;
    cb.config.ledOnDuration = CFG_LED_ON_DURATION_DEF;
    cb.config.voltStopLevel = CFG_VOLT_STOP_LEVEL_DEF;
    cb.config.debugBits = CFG_DEBUG_BITS_DEF;

    /* calc and assign the checksum */
    cb.checksum = calcCheckSum((char*)&cb.config, sizeof(ConfigData));

    /* erase the old config */
    memErase();

    /* write the new config */
    memWrite((char*)&cb, sizeof(cb));
    
    return 0;
}


int cfgSetBootFlag()
{
    ConfigBlock cb;
    
    /* clear out the config block */
    memset((char*)&cb, 0, sizeof(ConfigBlock));

    /* set the boot flag */
    cb.config.bootFlag = CFG_ACTIVE_BOOT_FLAG;

    /* calc and assign the checksum */
    cb.checksum = calcCheckSum((char*)&cb.config, sizeof(ConfigData));

    /* erase the old config */
    memErase();

    /* write the new config */
    memWrite((char*)&cb, sizeof(cb));
    
    return 0;
}


unsigned int calcCheckSum(char* buff, int size)
{
    int i;
    unsigned int cs = 0;

    for (i = 0; i < size; i++)
        cs += buff[i];

    return cs;
}

