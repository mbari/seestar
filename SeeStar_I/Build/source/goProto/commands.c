/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include <stddef.h>
#include "actions.h"
#include "commands.h"

/****************************************************************************/
/*                            command structures                            */
/****************************************************************************/

const CmdStruct cmdsRoot[] =
{
    {"GET",                 actGetCmd                   },
    {"SET",                 actSetCmd                   },
    {"CLEAR|CLR",           actClrCmd                   },
    {"EEPROG",              actEEProgCmd                },
    {"EEREAD",              actEEReadCmd                },
    {"CAM ON",              actCamOnCmd                 },
    {"CAM OFF",             actCamOffCmd                },
    {"SNAPHI",              actSnapHiCmd                },
    {"SNAPLO",              actSnapLoCmd                },
    {"VID ON",              actVidOnCmd                 },
    {"VID OFF",             actVidOffCmd                },
    {"LED ON",              actLedOnCmd                 },
    {"LED OFF",             actLedOffCmd                },
    {"PWR ON",              actPwrOnCmd                 },
    {"PWR OFF",             actPwrOffCmd                },
    {"TIME",                actTimeCmd                  },
    {"HELP|?",              actHelpCmd                  },
    {"RESET",               actResetCmd                 },
    {"SAVE",                actSaveCmd                  },
    {"PARAM",               actParamCmd                 },
    {"DEFAULT",             actDefaultCmd               },
    {"SAMPLE NOW",          actSampleNowCmd             },
    {"UPTIME",              actUptimeCmd                },
    {"ERASE MEM",           actMemEraseCmd              },
    {"WRITE MEM",           actMemWriteCmd              },
    {"READ MEM",            actMemReadCmd               },

    /* debug command(s) below */                        
    {"TEST1",               actTest1Cmd                 },
    {"TEST2",               actTest2Cmd                 },
    {"TEST3",               actTest3Cmd                 },
    {"TEST4",               actTest4Cmd                 },
    {"",                    NULL                        }
};
                                                    
const CmdStruct cmdsGet[] =                         
{                                                   
    {"LED",                 actGetLedCmd                },
    {"DEBUG",               actGetDebugCmd              },
    {"POWER",               actGetPowerCmd              },
    {"",                    NULL                        }
};                                                 
                                                   
const CmdStruct cmdsSet[] =                        
{
    {"TIME",                actSetTimeCmd               },
    {"DEBUG",               actSetDebugCmd              },
    {"AUTO RUN MODE",       actSetAutoRunModeCmd        },
    {"AUTO RUN DELAY",      actSetAutoRunDelayCmd       },
    {"AUTO RUN DURATION",   actSetAutoRunDurationCmd    },
    {"SAMPLE MODE",         actSetSampleModeCmd         },
    {"SAMPLE INTERVAL",     actSetSampleIntervalCmd     },
    {"SAMPLE DURATION",     actSetSampleDurationCmd     },
    {"LED ON DELAY",        actSetLedOnDelayCmd         },
    {"LED ON DURATION",     actSetLedOffDelayCmd        },
    {"VOLT STOP LEVEL",     actSetVoltStopLevelCmd      },
    {"",                    NULL                        }
};

const CmdStruct cmdsClr[] =
{
    {"DEBUG",               actClrDebugCmd              },
    {"",                    NULL                        }
};

