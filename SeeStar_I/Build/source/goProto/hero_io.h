/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/
/* Routines derived from Glen Akins gptestbrd01.c program                   */
/* (See http://bikerglen.com/ )                                             */
/****************************************************************************/
#ifndef HERO_IO_H
#define HERO_IO_H

/* outputs */
#define GP_TRIG     _LATC0
#define GP_ID2      _LATC2
#define GP_ID3      _LATC3
#define GP_MODE     _LATC5

#define CAM_ON_OFF  _LATA0

/* inputs */
#define GP_ID1      _RC1
#define GP_VADPT    _RC4

void hroInit();

void hroDebugOn();
void hroDebugOff();
int hroDebugState();

void hroPwrOn();
void hroPwrOff();

void hroCamOn();
void hroCamOff();

int hroSnapHi();
int hroSnapLo();

void hroStartVid();
void hroStopVid();

int hroGetCamState();
int hroGetId1State();

void hroMsDelay(unsigned int ms);


#endif
