/****************************************************************************/
/* Created 2010 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

/* general errors */
#define ERR_SUCCESS             0

#define ERR_FAILURE             1
#define ERR_EMPTY_LINE          2
#define ERR_NO_LINE             3
#define ERR_NO_COMMAND          4
#define ERR_NO_SUB_COMMAND      5


