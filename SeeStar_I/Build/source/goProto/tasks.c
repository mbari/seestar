/****************************************************************************/
/* Created 2011 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#include <stdio.h>

#include "tasks.h"
#include "analog.h"
#include "ex_rtc.h"
#include "serial.h"
#include "config.h"
#include "hero_io.h"
#include "sys_defs.h"
#include "sys_timer.h"
#include "data_flash.h"
#include <p24fxxxx.h>

/* debug message variables */
static char tskBuff[128];
static int tskDebugMode = FALSE;

/* task configuration data */
static ConfigData tskConfigData;

/* start time of the sampling task*/
static long tskEpochStartTime = 0L;

/* auto run state variables */
static int tskAutoRunStarted = FALSE;
static int tskAutoRunStopped = FALSE;

/* sampling task state variables */
static int tskReadyToSample = FALSE;
static int tskSamplingEnabled = FALSE;
static long tskSampleCount = 0L;
static long tskNextSampleTime = 0L;

/* generic state machine delay constants */
#define TSK_PWR_ON_DELAY        2000
#define TSK_CAM_ON_1_DELAY      9000
#define TSK_CAM_ON_2_DELAY      4000
#define TSK_CAM_OFF_DELAY       9000
#define TSK_LED_ON_DELAY        100
#define TSK_SNAP_HI_DELAY       500
#define TSK_REC_STOP_DELAY      2000

/* generic state machine variables */
static Timer tskStateTimer;
static unsigned long tskSampleDuration = 0L;

/****************************************************/
/* hero 2 pic state machine variables and constanst */
/****************************************************/
#define TSK_PIC_IDLE            0
#define TSK_PIC_PWR_ON          1
#define TSK_PIC_CAM_ON_1        2
#define TSK_PIC_CAM_ON_1_DELAY  3
#define TSK_PIC_CAM_ON_1_CHECK  4
#define TSK_PIC_CAM_ON_2        5
#define TSK_PIC_CAM_ON_2_DELAY  6
#define TSK_PIC_LED_ON          7
#define TSK_PIC_SNAP_HI         8
#define TSK_PIC_LED_OFF         9
#define TSK_PIC_CAM_OFF         10
#define TSK_PIC_CAM_OFF_DELAY   11
#define TSK_PIC_PWR_OFF         12

static int tskPicState = TSK_PIC_IDLE;
static int tskPicOnRetryCnt = 0;
static int tskPicOnRetryTotal = 0;
static unsigned long tskPicLoopCount = 0;

/****************************************************/
/* hero 2 vid state machine variables and constants */
/****************************************************/
#define TSK_VID_IDLE            0
#define TSK_VID_PWR_ON          1
#define TSK_VID_CAM_ON_1        2
#define TSK_VID_CAM_ON_1_DELAY  3
#define TSK_VID_CAM_ON_1_CHECK  4
#define TSK_VID_REC_ON          5
#define TSK_VID_REC_SAMPLE      6
#define TSK_VID_REC_OFF         7
#define TSK_VID_REC_OFF_DELAY   8
#define TSK_VID_CAM_OFF         9
#define TSK_VID_CAM_OFF_DELAY   10
#define TSK_VID_PWR_OFF         11

static int tskVidState = TSK_VID_IDLE;
static int tskVidOnRetryCnt = 0;
static int tskVidOnRetryTotal = 0;
static unsigned long tskVidLoopCount = 0;

/************************************************/
/* hero 3 state machine variables and constants */
/************************************************/
#define TSK_HR3_IDLE    0
#define TSK_HR3_PWR_ON  1
#define TSK_HR3_CAM_ON  2
#define TSK_HR3_LED_ON  3
#define TSK_HR3_LED_OFF 4
#define TSK_HR3_PWR_OFF 5

static Timer tskLedTimer;
static int tskHr3State = TSK_HR3_IDLE;
static unsigned long tskHr3LoopCount = 0L;
static unsigned long tskLedTimerDelay = 0L;

/* local fucntion prototypes */
static void tskPicStateMachine();
static void tskVidStateMachine();
static void tskHr3StateMachine();
static void tskDebugMsg(char* dbg_msg, int ts);
static void tskLogMsg(char* log_msg);


void tskInit()
{
    /* get task start time */
    tskEpochStartTime = rtcEpochTime();

    /* create all time state and interval timers */
    tmrCreate(&tskLedTimer);
    tmrStart(&tskLedTimer);

    tmrCreate(&tskStateTimer);
    tmrStart(&tskStateTimer);

    /* init config data with defaults */
    tskConfigData.autoRunMode = CFG_AUTO_RUN_MODE_DEF;
    tskConfigData.autoRunDelay = CFG_AUTO_RUN_DELAY_DEF;
    tskConfigData.autoRunDuration = CFG_AUTO_RUN_DURATION_DEF;
    tskConfigData.sampleMode = CFG_SAMPLE_MODE_DEF;
    tskConfigData.sampleInterval = CFG_SAMPLE_INTERVAL_DEF;
    tskConfigData.sampleDuration = CFG_SAMPLE_DURATION_DEF;
    tskConfigData.ledOnDelay = CFG_LED_ON_DELAY_DEF;
    tskConfigData.ledOnDuration = CFG_LED_ON_DURATION_DEF;
    tskConfigData.voltStopLevel = CFG_VOLT_STOP_LEVEL_DEF;
}

void tskDebugOn()
{
    tskDebugMode = TRUE;
}

void tskDebugOff()
{
    tskDebugMode = FALSE;
}

int tskDebugState()
{
    return tskDebugMode;
}

void tskExecute()
{
    /* read epoch_time at the top of the task loop*/
    long epoch_time = rtcEpochTime();
    /* calculate delays here */
    long auto_run_delay = tskConfigData.autoRunDelay + tskEpochStartTime;
    long auto_run_duration = auto_run_delay + tskConfigData.autoRunDuration;

    /* check for auto-run start */
    if ( (tskAutoRunStarted == FALSE) &&
         (tskConfigData.autoRunMode == ON) && 
         (epoch_time >= auto_run_delay) )
    {
        tskDebugMsg("tskAutoRunStarted = TRUE", TRUE);

        /* start the sample task here */
        tskEnableSampling();
        tskAutoRunStarted = TRUE;
    }

    /* check for auto-run stop */
    if ( (tskAutoRunStopped == FALSE) && 
         (tskConfigData.autoRunMode == ON) && 
         (epoch_time >= auto_run_duration ) )
    {
        tskDebugMsg("tskAutoRunStopped = TRUE", TRUE);

        /* stop the sample task here */
        tskDisableSampling();
        tskAutoRunStopped = TRUE;
    }

    /* see if it's time to sample */
    if ( tskSamplingEnabled && (epoch_time >= tskNextSampleTime) ) 
    {
        ++tskSampleCount;
        tskReadyToSample = TRUE;
        tskNextSampleTime += tskConfigData.sampleInterval;
        sprintf(tskBuff, "tskSampleCount = %ld", tskSampleCount);
        tskDebugMsg(tskBuff, TRUE);
    }

    /* run the appropriate state machine */
    if ( tskConfigData.sampleMode == CFG_SAMPLE_MODE_PIC )
        tskPicStateMachine(); 

    if ( tskConfigData.sampleMode == CFG_SAMPLE_MODE_VID )
        tskVidStateMachine(); 

    if ( tskConfigData.sampleMode == CFG_SAMPLE_MODE_HR3 )
        tskHr3StateMachine(); 

    return;
}

void tskSampleNow()
{
    tskReadyToSample = TRUE;
    return;
}

void tskEnableSampling()
{
    tskSamplingEnabled = TRUE;
    tskNextSampleTime = rtcEpochTime();
    sprintf(tskBuff, "tskNextSampleTime = %ld", tskNextSampleTime);
    tskDebugMsg(tskBuff, TRUE);

    return;
}

void tskDisableSampling()
{
    tskSamplingEnabled = FALSE;
    return;
}

static void tskPicStateMachine()
{
    double bus_power = 0.0;
    double bus_current = 0.0;
    double bus_voltage = 0.0;

    /* if the task is ready and running grab a pic at the given interval */
    if ( (tskReadyToSample) && (tskPicState == TSK_PIC_IDLE) )
    {
        tmrClear(&tskStateTimer);
        tskReadyToSample = FALSE;
        tskPicState = TSK_PIC_PWR_ON;
        tskDebugMsg("tskPicState = TSK_PIC_PWR_ON", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_IDLE State                  */
    /****************************************/
    if ( tskPicState == TSK_PIC_IDLE )
        return;


    /****************************************/
    /*  TSK_PIC_PWR_ON State                */
    /****************************************/
    if ( tskPicState == TSK_PIC_PWR_ON )
    {
        /* turn on camera power */
        hroPwrOn();

        /* switch states after power up delay */
        if ( tmrRead(&tskStateTimer) > TSK_PWR_ON_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskPicState = TSK_PIC_CAM_ON_1;
            tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_1", TRUE);

            /* count how many times you go through the state machine */
            ++tskPicLoopCount;

            /* clear out the retry count */
            tskPicOnRetryCnt = 0;

            /* measure BUS1 analog values here */
            sprintf(tskBuff, "BUS1, %lu, %u, %u",tskPicLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_PIC_CAM_ON_1 State              */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_ON_1 )
    {
        hroCamOn();
        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_CAM_ON_1_DELAY;
        tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_1_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_CAM_ON_1_DELAY State        */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_ON_1_DELAY )
    {
        /* switch states after cam on delay */
        if ( tmrRead(&tskStateTimer) > TSK_CAM_ON_1_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskPicState = TSK_PIC_CAM_ON_1_CHECK;
            tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_1_CHECK", TRUE);

            /* measure BUS2 analog values here */
            sprintf(tskBuff, "BUS2, %lu, %u, %u",tskPicLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_PIC_CAM_ON_1_CHECK State        */
    /****************************************/
#define HR2_ON_1_WATTS      1.2
#define HR2_ON_RETRY_MAX    3

    if ( tskPicState == TSK_PIC_CAM_ON_1_CHECK )
    {
        /* calculate hero power */
        bus_current = (double)anaMilliAmps();
        bus_voltage = (double)anaMilliVolts();

        bus_current /= 1000.0;
        bus_voltage /= 1000.0;
        bus_power = bus_current * bus_voltage; 

        /* check camera power consumption */
        if ( bus_power > HR2_ON_1_WATTS )
        {
            sprintf(tskBuff,"bus_power = %g Watts", bus_power);
            tskDebugMsg(tskBuff, TRUE);

            tskPicState = TSK_PIC_CAM_ON_2;
            tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_2", TRUE);
        }
        else
        {
            ++tskPicOnRetryCnt;
            ++tskPicOnRetryTotal;
            sprintf(tskBuff, "ERR: tskPicOnRetryCnt = %d, bus_power = %g Watts", 
                    tskPicOnRetryCnt, bus_power);
            tskDebugMsg(tskBuff, TRUE);
            tskPicState = TSK_PIC_CAM_ON_1;
            tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_1", TRUE);
        }

        /* if you've exceeded the retry count just give up */
        if ( tskPicOnRetryCnt > HR2_ON_RETRY_MAX)
        {
            sprintf(tskBuff, "ERR: tskPicOnRetryCnt = %d > HRO_ON_RETRY_MAX", 
                    tskPicOnRetryCnt);
            tskDebugMsg(tskBuff, TRUE);

            tskPicState = TSK_PIC_CAM_OFF;
            tskDebugMsg("tskPicState = TSK_PIC_CAM_OFF", TRUE);
        }
    }

    /****************************************/
    /*  TSK_PIC_CAM_ON_2 State              */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_ON_2 )
    {
        hroCamOn();
        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_CAM_ON_2_DELAY;
        tskDebugMsg("tskPicState = TSK_PIC_CAM_ON_2_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_CAM_ON_2_DELAY State        */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_ON_2_DELAY )
    {
        /* switch states after cam on delay */
        if ( tmrRead(&tskStateTimer) > TSK_CAM_ON_2_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskPicState = TSK_PIC_LED_ON;
            tskDebugMsg("tskPicState = TSK_PIC_LED_ON", TRUE);

            /* measure BUS3 analog values here */
            sprintf(tskBuff, "BUS3, %lu, %u, %u",tskPicLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_PIC_LED_ON State                */
    /****************************************/
    if ( tskPicState == TSK_PIC_LED_ON )
    {
        /* LED on */
        _LATC7 = 1;

        /* wait for LED */
        tmrDelayMs(TSK_LED_ON_DELAY);

        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_SNAP_HI;
        tskDebugMsg("tskPicState = TSK_PIC_SNAP_HI", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_SNAP_HI State               */
    /****************************************/
    if ( tskPicState == TSK_PIC_SNAP_HI )
    {
        hroSnapHi();

        /* wait snap hi to finish LED */
        tmrDelayMs(TSK_SNAP_HI_DELAY);

        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_LED_OFF;
        tskDebugMsg("tskPicState = TSK_PIC_LED_OFF", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_LED_OFF State               */
    /****************************************/
    if ( tskPicState == TSK_PIC_LED_OFF )
    {
        /* LED off */
        _LATC7 = 0;

        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_CAM_OFF;
        tskDebugMsg("tskPicState = TSK_PIC_CAM_OFF", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_CAM_OFF State               */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_OFF )
    {
        hroCamOff();
        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_CAM_OFF_DELAY;
        tskDebugMsg("tskPicState = TSK_PIC_CAM_OFF_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_PIC_CAM_OFF_DELAY State         */
    /****************************************/
    if ( tskPicState == TSK_PIC_CAM_OFF_DELAY )
    {
        /* wait for a bit before cutting power */
        if ( tmrRead(&tskStateTimer) > TSK_CAM_OFF_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskPicState = TSK_PIC_PWR_OFF;
            tskDebugMsg("tskPicState = TSK_PIC_PWR_OFF", TRUE);
            /* measure BUS4 analog values here */
            sprintf(tskBuff, "BUS4, %lu, %u, %u",tskPicLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_PIC_PWR_OFF State               */
    /****************************************/
    if ( tskPicState == TSK_PIC_PWR_OFF )
    {
        /* turn off camera power */
        hroPwrOff();

        tmrClear(&tskStateTimer);
        tskPicState = TSK_PIC_IDLE;
        tskDebugMsg("tskPicState = TSK_PIC_IDLE", TRUE);
    }

    return;
}

static void tskVidStateMachine()
{
    double bus_power = 0.0;
    double bus_current = 0.0;
    double bus_voltage = 0.0;

    /* if the task is ready and running grab a vid at the given interval */
    if ( (tskReadyToSample) && (tskVidState == TSK_VID_IDLE) )
    {
        tmrClear(&tskStateTimer);
        tskReadyToSample = FALSE;
        tskVidState = TSK_VID_PWR_ON;
        tskDebugMsg("tskVidState = TSK_VID_PWR_ON", TRUE);
    }

    /****************************************/
    /*  TSK_VID_IDLE State                  */
    /****************************************/
    if ( tskVidState == TSK_VID_IDLE )
        return;

    /****************************************/
    /*  TSK_VID_PWR_ON State                */
    /****************************************/
    if ( tskVidState == TSK_VID_PWR_ON )
    {
        /* turn on camera power */
        hroPwrOn();

        /* switch states after power up delay */
        if ( tmrRead(&tskStateTimer) > TSK_PWR_ON_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskVidState = TSK_VID_CAM_ON_1;
            tskDebugMsg("tskVidState = TSK_VID_CAM_ON_1", TRUE);

            /* count how many times you go through the state machine */
            ++tskVidLoopCount;

            /* clear out the retry count */
            tskVidOnRetryCnt = 0;

            /* measure BUS1 analog values here */
            sprintf(tskBuff, "BUS1, %lu, %u, %u",tskVidLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_VID_CAM_ON_1 State              */
    /****************************************/
    if ( tskVidState == TSK_VID_CAM_ON_1 )
    {
        hroCamOn();
        tmrClear(&tskStateTimer);
        tskVidState = TSK_VID_CAM_ON_1_DELAY;
        tskDebugMsg("tskVidState = TSK_VID_CAM_ON_1_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_VID_CAM_ON_1_DELAY State        */
    /****************************************/
    if ( tskVidState == TSK_VID_CAM_ON_1_DELAY )
    {
        /* switch states after cam on delay */
        if ( tmrRead(&tskStateTimer) > TSK_CAM_ON_1_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskVidState = TSK_VID_CAM_ON_1_CHECK;
            tskDebugMsg("tskVidState = TSK_VID_CAM_ON_1_CHECK", TRUE);

            /* measure BUS2 analog values here */
            sprintf(tskBuff, "BUS2, %lu, %u, %u",tskVidLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_VID_CAM_ON_1_CHECK State        */
    /****************************************/
    if ( tskVidState == TSK_VID_CAM_ON_1_CHECK )
    {
        /* calculate hero power */
        bus_current = (double)anaMilliAmps();
        bus_voltage = (double)anaMilliVolts();

        bus_current /= 1000.0;
        bus_voltage /= 1000.0;
        bus_power = bus_current * bus_voltage; 

        /* check camera power consumption */
        if ( bus_power > HR2_ON_1_WATTS )
        {
            sprintf(tskBuff,"bus_power = %g Watts", bus_power);
            tskDebugMsg(tskBuff, TRUE);

            tskVidState = TSK_VID_REC_ON;
            tskDebugMsg("tskVidState = TSK_VID_REC_ON", TRUE);
        }
        else
        {
            ++tskVidOnRetryCnt;
            ++tskVidOnRetryTotal;
            sprintf(tskBuff, "ERR: tskVidOnRetryCnt = %d, bus_power = %g Watts", 
                    tskVidOnRetryCnt, bus_power);
            tskDebugMsg(tskBuff, TRUE);
            tskVidState = TSK_VID_CAM_ON_1;
            tskDebugMsg("tskVidState = TSK_VID_CAM_ON_1", TRUE);
        }

        /* if you've exceeded the retry count just give up */
        if ( tskVidOnRetryCnt > HR2_ON_RETRY_MAX)
        {
            sprintf(tskBuff, "ERR: tskVidOnRetryCnt = %d > HRO_ON_RETRY_MAX", 
                    tskVidOnRetryCnt);
            tskDebugMsg(tskBuff, TRUE);

            tskVidState = TSK_VID_CAM_OFF;
            tskDebugMsg("tskVidState = TSK_VID_CAM_OFF", TRUE);
        }
    }

    /****************************************/
    /*  TSK_VID_REC_ON State                */
    /****************************************/
    if ( tskVidState == TSK_VID_REC_ON )
    {
        /* lights, camera, action!!!*/

        /* turn on lights */
        _LATC7 = 1;

        /*start recording */
        hroStartVid();
        tmrClear(&tskStateTimer);

        tskSampleDuration = (tskConfigData.sampleDuration * 1000);
        tskVidState = TSK_VID_REC_SAMPLE;
        tskDebugMsg("tskVidState = TSK_VID_REC_SAMPLE", TRUE);
    }


    /****************************************/
    /*  TSK_VID_REC_SAMPLE State            */
    /****************************************/
    if ( tskVidState == TSK_VID_REC_SAMPLE )
    {
        /* switch states after power up delay */
        if ( tmrRead(&tskStateTimer) > tskSampleDuration )
        {
            tmrClear(&tskStateTimer);
            tskVidState = TSK_VID_REC_OFF;
            tskDebugMsg("tskVidState = TSK_VID_REC_OFF", TRUE);
        }
    }


    /****************************************/
    /*  TSK_VID_REC_OFF State               */
    /****************************************/
    if ( tskVidState == TSK_VID_REC_OFF )
    {
        /* LED off */
        _LATC7 = 0;

        /*stop recording */
        hroStopVid();

        tmrClear(&tskStateTimer);
        tskVidState = TSK_VID_REC_OFF_DELAY;
        tskDebugMsg("tskVidState = TSK_VID_REC_OFF_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_VID_REC_OFF_DELAY State         */
    /****************************************/
    if ( tskVidState == TSK_VID_REC_OFF_DELAY )
    {
        if ( tmrRead(&tskStateTimer) > TSK_REC_STOP_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskVidState = TSK_VID_CAM_OFF;
            tskDebugMsg("tskVidState = TSK_VID_CAM_OFF", TRUE);
        }
    }

    /****************************************/
    /*  TSK_VID_CAM_OFF State               */
    /****************************************/
    if ( tskVidState == TSK_VID_CAM_OFF )
    {
        hroCamOff();
        tmrClear(&tskStateTimer);
        tskVidState = TSK_VID_CAM_OFF_DELAY;
        tskDebugMsg("tskVidState = TSK_VID_CAM_OFF_DELAY", TRUE);
    }

    /****************************************/
    /*  TSK_VID_CAM_OFF_DELAY State         */
    /****************************************/
    if ( tskVidState == TSK_VID_CAM_OFF_DELAY )
    {
        /* wait for a bit before cutting power */
        if ( tmrRead(&tskStateTimer) > TSK_CAM_OFF_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskVidState = TSK_VID_PWR_OFF;
            tskDebugMsg("tskVidState = TSK_VID_PWR_OFF", TRUE);
            /* measure BUS3 analog values here */
            sprintf(tskBuff, "BUS3, %lu, %u, %u",tskVidLoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskDebugMsg(tskBuff, TRUE);
        }
    }

    /****************************************/
    /*  TSK_VID_PWR_OFF State               */
    /****************************************/
    if ( tskVidState == TSK_VID_PWR_OFF )
    {
        /* turn off camera power */
        hroPwrOff();

        tmrClear(&tskStateTimer);
        tskVidState = TSK_VID_IDLE;
        tskDebugMsg("tskVidState = TSK_VID_IDLE", TRUE);
    }

    return;
}

static void tskHr3StateMachine()
{
    /* if the task is ready and running grab a sample at the given interval */
    if ( (tskReadyToSample) && (tskHr3State == TSK_HR3_IDLE) )
    {
        tmrClear(&tskStateTimer);
        tskReadyToSample = FALSE;
        tskHr3State = TSK_HR3_PWR_ON;
        tskDebugMsg("tskHr3State = TSK_HR3_PWR_ON", TRUE);
    }

    /****************************************/
    /*  TSK_HR3_IDLE State                  */
    /****************************************/
    if ( tskHr3State == TSK_HR3_IDLE )
        return;


    /****************************************/
    /*  TSK_HR3_PWR_ON State                */
    /****************************************/
    if ( tskHr3State == TSK_HR3_PWR_ON )
    {
        /* turn on camera power */
        hroPwrOn();

        /* switch states after power up delay */
        if ( tmrRead(&tskStateTimer) > TSK_PWR_ON_DELAY )
        {
            tmrClear(&tskStateTimer);
            tskHr3State = TSK_HR3_CAM_ON;
            tskDebugMsg("tskHr3State = TSK_HR3_CAM_ON", TRUE);

            /* count how many times you go through the state machine */
            ++tskHr3LoopCount;
        }
    }

    /****************************************/
    /*  TSK_HR3_CAM_ON State                */
    /****************************************/
    if ( tskHr3State == TSK_HR3_CAM_ON )
    {
        hroCamOn();
        tmrClear(&tskLedTimer);
        tmrClear(&tskStateTimer);
        tskHr3State = TSK_HR3_LED_ON;
        tskLedTimerDelay = (tskConfigData.ledOnDelay * 1000);
        tskSampleDuration = (tskConfigData.sampleDuration * 1000);
        tskDebugMsg("tskHr3State = TSK_HR3_LED_ON", TRUE);
    }

    /****************************************/
    /*  TSK_HR3_CAM_ON State                */
    /****************************************/
    if ( tskHr3State == TSK_HR3_LED_ON ) 
    {
        if ( tmrRead(&tskLedTimer) > tskLedTimerDelay ) 
        {
            /* only turn on the LED if the on duration is set */
            if ( tskConfigData.ledOnDuration ) 
            {
                /* LED on */
                _LATC7 = 1;
            }

            /* setup the delay and state for LED duration */
            tmrClear(&tskLedTimer);
            tskHr3State = TSK_HR3_LED_OFF;
            tskLedTimerDelay = (tskConfigData.ledOnDuration * 1000);
            tskDebugMsg("tskHr3State = TSK_HR3_LED_OFF", TRUE);
        }
    }

    /****************************************/
    /*  TSK_HR3_LED_OFF State               */
    /****************************************/
    if ( tskHr3State == TSK_HR3_LED_OFF )
    {
        if ( tmrRead(&tskLedTimer) > tskLedTimerDelay ) 
        {
            /* LED off */
            _LATC7 = 0;

            /* switch the state to back to idle*/
            tskHr3State = TSK_HR3_PWR_OFF;
            tskDebugMsg("tskHr3State = TSK_HR3_PWR_OFF", TRUE);

            /* log BUS analog values here */
            sprintf(tskBuff, "HR3.1, %lu, %u, %u, ",tskHr3LoopCount, 
                    anaMilliAmps(), anaMilliVolts());
            tskLogMsg(tskBuff);

        }
    }

    /****************************************/
    /*  TSK_HR3_PWR_OFF State               */
    /****************************************/
    if ( tskHr3State == TSK_HR3_PWR_OFF )
    {
        /* wait for a bit before cutting power */
        if ( tmrRead(&tskStateTimer) > tskSampleDuration )
        {
            /* turn off camera power */
            hroPwrOff();
            tmrClear(&tskStateTimer);
            tskHr3State = TSK_HR3_IDLE;
            tskDebugMsg("tskHr3State = TSK_HR3_IDLE", TRUE);
        }
    }

    return;
}

/****************************************************************************/
/*                          Configuration settings                          */
/****************************************************************************/

unsigned int tskGetAutoRunMode()
{
    return tskConfigData.autoRunMode;
}

int tskSetAutoRunMode(unsigned int mode)
{
    /* check the param */
    if ( (mode != ON) && (mode != OFF) )
        return -1;

    /* set the param*/
    tskConfigData.autoRunMode = mode;

    return 0;
}

unsigned long tskGetAutoRunDelay()
{
    return tskConfigData.autoRunDelay;
}

int tskSetAutoRunDelay(unsigned long val)
{
    /* set the param */
    tskConfigData.autoRunDelay = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.autoRunDelay < CFG_AUTO_RUN_DELAY_MIN )
        tskConfigData.autoRunDelay = CFG_AUTO_RUN_DELAY_MIN; 

    if ( tskConfigData.autoRunDelay > CFG_AUTO_RUN_DELAY_MAX )
        tskConfigData.autoRunDelay = CFG_AUTO_RUN_DELAY_MAX;

    return 0;
}

unsigned long tskGetAutoRunDuration()
{
    return tskConfigData.autoRunDuration;
}

int tskSetAutoRunDuration(unsigned long val)
{
    /* set the param */
    tskConfigData.autoRunDuration = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.autoRunDuration < CFG_AUTO_RUN_DURATION_MIN )
        tskConfigData.autoRunDuration = CFG_AUTO_RUN_DURATION_MIN; 

    if ( tskConfigData.autoRunDuration > CFG_AUTO_RUN_DURATION_MAX )
        tskConfigData.autoRunDuration = CFG_AUTO_RUN_DURATION_MAX;

    return 0;
}

unsigned int tskGetSampleMode()
{
    return tskConfigData.sampleMode;
}

int tskSetSampleMode(unsigned int mode)
{
    /* make sure the mode of opertation is valid*/
    switch ( mode ) 
    {
        case CFG_SAMPLE_MODE_OFF: break;
        case CFG_SAMPLE_MODE_PIC: break;
        case CFG_SAMPLE_MODE_VID: break;
        case CFG_SAMPLE_MODE_HR3: break;
        default: return -1;
    }

    /* set the mode of operation */
    tskConfigData.sampleMode = mode;

    return 0;
}

unsigned long tskGetSampleInterval()
{
    return tskConfigData.sampleInterval;
}

int tskSetSampleInterval(unsigned long val)
{
    /* set the param */
    tskConfigData.sampleInterval = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.sampleInterval < CFG_SAMPLE_INTERVAL_MIN )
        tskConfigData.sampleInterval = CFG_SAMPLE_INTERVAL_MIN; 

    if ( tskConfigData.sampleInterval > CFG_SAMPLE_INTERVAL_MAX )
        tskConfigData.sampleInterval = CFG_SAMPLE_INTERVAL_MAX;

    return 0;
}

unsigned long tskGetSampleDuration()
{
    return tskConfigData.sampleDuration;
}

int tskSetSampleDuration(unsigned long val)
{
    /* set the param */
    tskConfigData.sampleDuration = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.sampleDuration < CFG_SAMPLE_DURATION_MIN )
        tskConfigData.sampleDuration = CFG_SAMPLE_DURATION_MIN; 

    if ( tskConfigData.sampleDuration > CFG_SAMPLE_DURATION_MAX )
        tskConfigData.sampleDuration = CFG_SAMPLE_DURATION_MAX;

    return 0;
}

unsigned long tskGetLedOnDelay()
{
    return tskConfigData.ledOnDelay;
}

int tskSetLedOnDelay(unsigned long val)
{
    /* set the param */
    tskConfigData.ledOnDelay = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.ledOnDelay < CFG_LED_ON_DELAY_MIN )
        tskConfigData.ledOnDelay = CFG_LED_ON_DELAY_MIN; 

    if ( tskConfigData.ledOnDelay > CFG_LED_ON_DELAY_MAX )
        tskConfigData.ledOnDelay = CFG_LED_ON_DELAY_MAX;

    return 0;
}

unsigned long tskGetLedOnDuration()
{
    return tskConfigData.ledOnDuration;
}

int tskSetLedOnDuration(unsigned long val)
{
    /* set the param */
    tskConfigData.ledOnDuration = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.ledOnDuration < CFG_LED_ON_DURATION_MIN )
        tskConfigData.ledOnDuration = CFG_LED_ON_DURATION_MIN; 

    if ( tskConfigData.ledOnDuration > CFG_LED_ON_DURATION_MAX )
        tskConfigData.ledOnDuration = CFG_LED_ON_DURATION_MAX;

    return 0;
}

unsigned long tskGetVoltStopLevel()
{
    return tskConfigData.voltStopLevel;
}

int tskSetVoltStopLevel(unsigned long val)
{
    /* set the param */
    tskConfigData.voltStopLevel = val;

    /* if it's out of range, bring it in range */
    if ( tskConfigData.voltStopLevel < CFG_VOLT_STOP_LEVEL_MIN )
        tskConfigData.voltStopLevel = CFG_VOLT_STOP_LEVEL_MIN; 

    if ( tskConfigData.voltStopLevel > CFG_VOLT_STOP_LEVEL_MAX )
        tskConfigData.voltStopLevel = CFG_VOLT_STOP_LEVEL_MAX;

    return 0;
}

static void tskDebugMsg(char* dbg_msg, int ts)
{
    if ( tskDebugMode ) 
    {
        serPutString(SER_CONSOLE, dbg_msg);

        /* add a time stamp if ts is set */
        if ( ts ) 
        {
            serPutString(SER_CONSOLE, " [");
            rtcTimeDateStr(tskBuff);
            serPutString(SER_CONSOLE, tskBuff);
            serPutString(SER_CONSOLE, "]\r\n");
        }
    }

    return;
}


static void tskLogMsg(char* log_msg)
{
    dmPutString(log_msg);
    dmPutString(" [");
    rtcTimeDateStr(tskBuff);
    dmPutString(tskBuff);
    dmPutString("]\r\n");

    return;
}

