/****************************************************************************/
/* Created 2009 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef BUFFER_H
#define BUFFER_H

typedef struct
{
    unsigned char* data;
    int head;
    int tail;
    int ptr_mask;
} ByteBuffer;

/* buffer access and status functions */
int bufPut(unsigned char b, ByteBuffer* buff);
int bufGet(unsigned char* b, ByteBuffer* buff);

void bufClear(ByteBuffer* buff);
int bufIsFull(ByteBuffer* buff);
int bufIsEmpty(ByteBuffer* buff);
int bufAvailable(ByteBuffer* buff);

#endif
