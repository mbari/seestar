/****************************************************************************/
/* Created 2011 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/

#ifndef EX_RTC_H
#define EX_RTC_H

typedef struct
{
    int seconds;
    int minutes;
    int hours;
    int day;
    int date;
    int month;
    int year;
} RtcTime;


void rtcInit();
int rtcSet(RtcTime* time);
int rtcRead(RtcTime* time);
int rtcTimeDateStr(char* tm_str);
int rtcTimeStampStr(char* tm_str);
long rtcEpochTime();

#endif

