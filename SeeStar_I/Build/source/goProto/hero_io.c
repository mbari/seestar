/****************************************************************************/
/* Created 2012 (Anno Domini)                                               */
/****************************************************************************/
/* MBARI SeeStar (See http://www.mbari.org/seestar/) is licensed under a    */
/* Creative Commons Attribution-ShareAlike 4.0 International License.       */
/*                                                                          */
/* You should have received a copy of the license along with this work. If  */
/* not, see <http://creativecommons.org/licenses/by-sa/4.0/>.               */
/*                                                                          */
/* Citation: F. Cazenave, C. Kecy, M. Risi, S.H.D. Haddock (in press)       */
/*    "SeeStar: a low-cost, modular, and open-source camera system          */
/*    for subsea observations", IEEE Oceans 2014                            */
/*                                                                          */
/****************************************************************************/
/* Routines derived from Glen Akins gptestbrd01.c program                   */
/* (See http://bikerglen.com/ )                                             */
/****************************************************************************/
#include <p24fxxxx.h>
#include <stdio.h>
#include <string.h>

#include "serial.h"
#include "ex_rtc.h"
#include "hero_io.h"
#include "sys_defs.h"

/* debug message variables */
static char hroBuff[128];
static int hroDebugMode = FALSE;
static void hroDebugMsg(char* dbg_msg, int ts);

void hroInit()
{
    /* initialize hero bus I/O */
    GP_MODE = 0;
    Nop();
    GP_TRIG = 1;
    Nop();
    GP_ID1 = 1;
    Nop();
    GP_ID2 = 1;
    Nop();
    GP_ID3 = 0;
    Nop();
    
    return;
}

void hroDebugOn()
{
    hroDebugMode = TRUE;
}

void hroDebugOff()
{
    hroDebugMode = FALSE;
}

int hroDebugState()
{
    return hroDebugMode;
}

void hroPwrOn()
{
    CAM_ON_OFF = 1;
    return;
}

void hroPwrOff()
{
    CAM_ON_OFF = 0;
    return;
}

void hroCamOn()
{
    hroDebugMsg("GP_MODE = 1", TRUE);
    GP_MODE = 1;

    /* wait a bit */
    hroMsDelay(1000);
    
    GP_MODE = 0;
    hroDebugMsg("GP_MODE = 0", TRUE);
    
    hroDebugMsg("hroCamOn() complete", TRUE);
    
    return;
}

void hroCamOff()
{
    hroDebugMsg("GP_MODE = 1", TRUE);
    GP_MODE = 1;

    /* wait a bit */
    hroMsDelay(3000);
    
    GP_MODE = 0;
    hroDebugMsg("GP_MODE = 0", TRUE);
    
    hroDebugMsg("hroCamOff() complete", TRUE);
    
    return;
}


#define MAX_SNAP_POLL_MS    5000
int hroSnapHi()
{
    int i = 0;

	/* set id3 high */
    GP_ID3 = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id2 low */
	GP_ID2 = 0;

	/* id1 will immediately go low to indicate the camera is busy
	after about 50 - 100 ms, id1 will return high to indicate
	the camera is ready */

	/* delay about 2ms to allow id1 to go low */
    hroMsDelay(2);

	/* wait for id1 to go high again */
	while (GP_ID1 == 0)
    {
        hroMsDelay(1);
        if ( ++i > MAX_SNAP_POLL_MS)
            break;
            
    }

	/* delay about 2ms to allow id1 to go low */
    hroMsDelay(2);
	
    /* pulse trigger low to take the picture */
	GP_TRIG = 0;

	/* delay about 2ms to make a short pulse */
    hroMsDelay(2);

	/* set trigger high to finish the pulse */
	GP_TRIG = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id2 high */
	GP_ID2 = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id3 low */
    GP_ID3 = 0;

    if ( i > MAX_SNAP_POLL_MS)
    {
        hroDebugMsg("ERR: hroSnapHi() timed out waiting for ID1", TRUE);
        return 1;
    }

    return 0;
}

int hroSnapLo()
{
    int i = 0;

	/* set id3 high */
    GP_ID3 = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id2 low */
	GP_ID2 = 0;

	/* id1 will immediately go high to indicate the camera is busy
	after about 50 - 100 ms, id1 will return low to indicate
	the camera is ready */

	/* delay about 2ms to allow id1 to go low */
    hroMsDelay(2);

	/* wait for id1 to go lo again */
	while (GP_ID1 != 0)
    {
        hroMsDelay(1);
        if ( ++i > MAX_SNAP_POLL_MS)
            break;
            
    }

	/* pulse trigger low to take the picture */
	GP_TRIG = 0;

	/* delay about 2ms to make a short pulse */
    hroMsDelay(2);

	/* set trigger high to finish the pulse */
	GP_TRIG = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id2 high */
	GP_ID2 = 1;

	/* delay about 20ms */
    hroMsDelay(20);

	/* set id3 low */
    GP_ID3 = 0;
    
    if ( i > MAX_SNAP_POLL_MS)
    {
        hroDebugMsg("ERR: hroSnapLo() timed out waiting for ID1", TRUE);
        return 1;
    }
    
    return 0;
}

void hroStartVid()
{
    GP_ID2 = 0;

    return;
}

void hroStopVid()
{
    GP_ID2 = 1;
    
    return;
}

int hroGetCamState()
{
    return ((int)GP_VADPT);
}

int hroGetId1State()
{
    return ((int)GP_ID1);
}

#define HRO_DELAY_LOOPS 305
/* calibrated for 3.6864 MHz OSC */
void hroMsDelay(unsigned int ms)
{
    unsigned int i, j;

    for (i = 0; i < ms; ++i)
    {
        j = HRO_DELAY_LOOPS;
        while ( j-- )
            Nop ();
    }
}

static void hroDebugMsg(char* dbg_msg, int ts)
{
    if ( hroDebugMode ) 
    {
        serPutString(SER_CONSOLE, dbg_msg);

        /* add a time stamp if ts is set */
        if ( ts ) 
        {
            serPutString(SER_CONSOLE, " [");
            rtcTimeDateStr(hroBuff);
            serPutString(SER_CONSOLE, hroBuff);
            serPutString(SER_CONSOLE, "]\r\n");
        }
    }

    return;
}


