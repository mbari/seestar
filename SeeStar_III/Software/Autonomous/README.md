# SeeStar version III Autonomous Software

The third generation SeeStar system software is currently under development.
This autonomous version is designed to be deployed for weeks or months on a battery.
It is based on the Sleepy Pi 2 module, which is very flexible, has a real-time clock and can power down 
between taking pictures making it a good low-powered option.

## Hardware prerequisites
- Sleepy Pi version 2 from [SpellFoundry](https://github.com/SpellFoundry/)
- [External Programming Adapter](https://spellfoundry.com/product/sleepy-pi-programming-adapter/)

## Software prerequisites
- Arduino development environment version 1.8.4 and the following library dependencies: 
- [SleepyPi2](https://github.com/SpellFoundry/SleepyPi2.git)
- [Time library](https://github.com/PaulStoffregen/Time)
- [Tiny Protocol library](https://github.com/lexus2k/arduino-protocol)
- [DS1374RTC library](https://github.com/SpellFoundry/DS1374RTC.git)
- [PCF8523 library](https://github.com/SpellFoundry/PCF8523.git)
- [Low power library](https://github.com/rocketscream/Low-Power.git) 
- [Timezone library](https://github.com/JChristensen/Timezone.git)

## Setup
In the autonomous mode, the SeeStar only uses the Arduino on board the Sleepy Pi and can be programmed as a
standalone board as detailed [here](https://spellfoundry.com/sleepy-pi/programming-sleepy-pi-standalone-board/).

Pin I/O is defined in the SeeStar.ino file in the deploy directory
A useful pin mapping diagram: [Sleepy Pi 2 I/O pinout diagram](https://spellfoundry.com/wp/wp-content/uploads/2016/11/Sleepy-Pi-2-Expansion-Annotated-2-1000.png)

# Install SleepyPi board to Arduino
Install the SleepyPi board settings to the Arduino development environment by opening a terminal window 
appending the contents to the board library. This will allow the Arduino environment to recognize the SleepyPy board.

The Arduino installation directory, depends on your operating system. This example is for Mac OSX. For Linux it might be /usr/local/arduino, Windows it is \Users\<username>\Documents\Arduino\, or it could be a custom location if you have setup one. 
Alternatively, you can skip this step and use the [Arduino FIO](http://arduino.cc/en/Main/ArduinoBoardFio) option that works as a target for the Sleepy Pi.
``` bash
    $ cd SeeStar_III/Software/Autonomous/deploy
    $ cat boards.txt >> /Applications/Arduino.app/Contents/Java/hardware/arduino/avr/boards.txt
```

More information on customizing Arduino can be found [here](https://playground.arduino.cc/Main/CustomizeArduinoIDE)

# Install library dependencies
The Arduino library installation directory depends on your operating system. This example is for Mac OSX. For Linux it might be /usr/local/arduino/libraries, Windows it might be \Users\<username>\Documents\Arduino\libraries, or it could be a custom location if you have set up one. 

To install the libraries:
 
 1.  Open a terminal window and fetch the library in that directory, e.g.

``` bash
    $ cd /Users/<username>/Documents/Arduino/libraries
    $ git clone https://github.com/lexus2k/arduino-protocol TinyProtocol 
    $ git clone https://github.com/rocketscream/Low-Power.git LowPower 
    $ git clone https://github.com/SpellFoundry/DS1374RTC.git 
    $ git clone https://github.com/SpellFoundry/PCF8523.git 
    $ git clone https://github.com/PaulStoffregen/Time 
    $ git clone https://github.com/JChristensen/Timezone
    $ git clone https://github.com/SpellFoundry/SleepyPi2
```
 2. Launch the Arduino development environment
  
 3. For each library, install by selecting the Sketch menu, then Add ZIP Library(you're not really adding a zip, but ignore that), then navigate to the folder the library was installed at.
 
 3. The libraries should now be installed and available in your Sketch->Include Libraries->Contributed Libraries section in the Arduino development environment.
        
### Helpful tip
*If you move a library, the library.properties file within the respective library subdirectory must also be removed from each directory in the library
for the Arduino IDE to catalog correctly. The Arduino IDE must also be restarted.*

## Run
The main sketch for running in Autonomous mode is [SeeStar.ino](deploy/src/SeeStar/SeeStar.ino) in the Autonomous/deploy/src/SeeStar directory. *Before* running that, there are a number of tests you should run to test your system. See the [test/](test) directory for details.

The Arduino sketch is basically [two simple state machines](img/state_machine.jpg)

The sketch will run a time-lapse for either the Sony QX1 or the Sony Cyber-shot DSC-RX10 camera.

 * Important to note 
    - By default, recording begins immediately.
    - By default, the sketch disables GUI communications to save space.  GUI communications may be enabled for a tethered deployment, or testing by setting the GUI_COMMS variable true.
    - The timezone is optional but must be set if you set specific start/end times or plan on using the GUI.
    - The real-time clock on the Sleepy Pi can be set with the [RTCSetTime.ino](Autonomous/test/src/SeeStar/RTCSetTime) sketch. Set to UTC only.  
    
* Edit SeeStar.ino and change these variables in the section CHANGE THESE FOR YOUR DEPLOYMENT:

| Variable | Description  |
|:------------- |:------------- |
| CFG_START_SECS | 0 to start immediately, or set start_datetime |
| CFG_END_SECS | 0 to run forever, or set end_datetime |
| start_datetime | (optional) only needed if setting starting time exactly. Format is (year, month, day, hour, minute, sec) in configured timezone LOCALDT/LOCALST |
| end_datetime | (optional) only needed if setting ending time exactly. Format is (year, month, day, hour, minute, sec) in configured timezone LOCALDT/LOCALST |
| LOCALDT | (optional) only needed if using GUI or setting the starting/ending time exactly. See (Timezone)[https://github.com/JChristensen/Timezone] for more info |
| LOCALST | (optional) only needed if using GUI or setting the starting/ending time exactly. See (Timezone)[https://github.com/JChristensen/Timezone] for more info |
| CFG_CAMERA_TYPE | Camera type QX1=Sony QX1 or DSCRX10=Sony Cyber-shot DSC-RX10 camera |
| CFG_RECORDING_MODE | STILL=still frame time-lapse or VIDEO=video time-lapse |
| CFG_INTERVAL | interval in timebase between the time-lapse recording |
| CFG_TIMEBASE | timebase between the time-lapse recording; valid values=eTB_SECOND, eTB_MINUTE, eTB_HOUR |
| CFG_VIDEO_DURATION_SECS |  default time for video capture (this is ignored in STILL mode) |
| CFG_POWER_MIN_SLEEPYPI_MILLVOLT | minimum voltage that SleepyPi can be powered with before shutting down. Leave this alone unless you plan on testing this with your system. This is a reasonable default. |
| CFG_POWER_MIN_MILLVOLT | default minimum voltage before doing controlled power down. Leave this alone unless you plan on testing this with your system. This is a reasonable default |
 
* Upload sketch
