/*
  Custom SeeStar Serial ProtoLight class build upon the TinyLightProtoLight. For testing purposes only.
  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
  To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
 */
 
#if defined(ARDUINO)
#   if ARDUINO >= 100
    #include "Arduino.h"
#   else
    #include "WProgram.h"
#   endif
#endif

#include "SeeStarLightProtocol.h"
 
namespace SeeStar {
  
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void ProtoLight::begin(write_block_cb_t writecb,
                  read_block_cb_t readcb)
{
    proto.begin(writecb, readcb);
}

void ProtoLight::end()
{
    proto.end();
}

int ProtoLight::write(char* buf, int size)
{
    return proto.write(buf, size);
}

int ProtoLight::read(char *buf, int size)
{
    return proto.read(buf, size);
}

#ifdef ARDUINO

int ProtoLight::writeToSerial(void *p, const uint8_t *b, int s)
{
    int result = m_serial->write(b, s);
    return result;
}

int ProtoLight::readFromSerial(void *p, uint8_t *b, int s)
{
    int length =  m_serial->readBytes(b, s);
    return length;
}


void ProtoLight::beginToSerial()
{
    m_serial->setTimeout( 100 );
    begin(writeToSerial, readFromSerial);
}

#endif

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

}  //  SeeStar namespace

