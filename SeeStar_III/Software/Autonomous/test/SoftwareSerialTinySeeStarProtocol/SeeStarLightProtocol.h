/*
  Custom SeeStar Serial ProtoLight class build upon the TinyLightProtoLight. For testing purposes only.
  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
  To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
 */

/**
 Custom SeeStar Serial ProtoLight class built upon the TinyLightProtoLight implementation for microcontrollers

 @file
 @brief SeeStar ProtoLight Arduino API
*/

#ifndef _SEESTAR_LIGHT_PROTOCOL_H_
#define _SEESTAR_LIGHT_PROTOCOL_H_

#include <TinyLightProtocol.h>
#include <TinyPacket.h>
#include <proto/tiny_light.h>

#ifdef ARDUINO
#   include <HardwareSerial.h>
#   include <SoftwareSerial.h>
#else
#   include <string.h>
#endif

namespace SeeStar {

/**
 *  ProtoLight class encapsulates ProtoLight functionality.
 */
class ProtoLight
{
public:
  /**
    * A constructor.
    * Uses pointer to generic Stream device to send/receive.
    */
    /*ProtoLight(Stream &dev) : m_serial(dev) {};*/
    ProtoLight() {};
    
    static Stream              *m_serial;
    
    /**
     * Initializes ProtoLight internal variables.
     * If you need to switch communication with other destination
     * point, you can call this method one again after calling end().
     * @param writecb - write function to some physical channel
     * @param readcb  - read function from some physical channel
     * @return None
     */
    void begin          (write_block_cb_t writecb,
                         read_block_cb_t readcb);

#ifdef ARDUINO
    /**
     * Initializes ProtoLight internal variables and redirects
     * communication through Arduino Serial connection (Serial).
     * @return None
     */
    void beginToSerial();

    static int writeToSerial(void *p, const uint8_t *b, int s);

    static int readFromSerial(void *p, uint8_t *b, int s);

#endif

    /**
     * Resets ProtoLight state.
     */
    void end            ();

    /**
     * Sends data block over communication channel.
     * @param buf - data to send
     * @param size - length of the data in bytes
     * @return negative value in case of error
     *         zero if nothing is sent
     *         positive - should be equal to size parameter
     */
    int  write          (char* buf, int size);

    /**
     * Reads data block from communication channel.
     * @param buf - buffer to place data read from communication channel
     * @param size - maximum size of the buffer in bytes.
     * @return negative value in case of error
     *         zero if nothing is read
     *         positive - number of bytes read from the channel
     */
    int  read           (char* buf, int size);


private:
    Tiny::ProtoLight    proto;
};


} // SeeStar namespace


#endif
