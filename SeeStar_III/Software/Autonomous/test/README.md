# SeeStar version III Test Sketches

This directory contains a collection of sketches that can be used to test different aspects of the SeeStar camera system.
 
| Sketch Directory | Description  |
|:------------- |:------------- |
| CamPowerOnOffTest  |  Turns on SeeStar DC-DC converter which applies power to the SeeStar camera then repeats. |
| CamTest  |  Simple serial based control to test the SeeStar Camera. Use with the Arduino Serial Monitor. |
| CamVideoTest  |  Turns on SeeStar Camera, takes a 30 second video clip, delays 5 seconds, then repeats. |
| ExternalPowerOffTest  |   Turns on/off external 3.3/5V power on a button press. |
| LightTest  | Turns on/off LED module |
| LowPowerTest | Loop to test baseline power consumption with no camera turned on in SeeStar module | 
| RTCSetTime | Sets the clock in response to serial port time message. Use with the Arduino Serial Monitor. |
| RTCTest | Reads the SleepyPi RTC and prints out the time in local and UTC format. |
| SensorSoftwareSerialTest | Sends a take sample command to the Sensor Module and prints a return code. |
| SoftwareSerial | Software serial test code. Software serial is used between the SeeStar and the Sensor Module |
| SoftwareSerialTinySeeStarProtocol |  TinyProtocol over Software Serial test code. TinyProtocol is used between the SeeStar camera system and the GUI |
| SerialTinySeeStarProtocolTest | Tiny Protocol over Serial test code |
| PacketTest | Test that formats the system status packet that will be sent over the serial port 
 to the GUI for updates. |
| SupplyVoltageTest | Reads and prints SleepyPi voltage |
| ToggleSensorRelayTest | Turns on/off sensor module | 
| WakePeriodicallyTest | Run wake/shutdown cycle on Arduino every 10 seconds |
