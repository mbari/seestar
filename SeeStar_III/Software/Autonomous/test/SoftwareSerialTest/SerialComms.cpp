#include "SerialComms.h"    


void SerialComms::gotoPos(int pos)
{
  _dev->write(pos);
}

void SerialComms::takeSample()
{
  _dev->write(0);
}

int SerialComms::computeCheckSum(char *string)
{
  unsigned int checkSum = 0; 
  for (char *stringPtr = string; *stringPtr != '\0'; stringPtr++){
    checkSum += *stringPtr;
  } 
  checkSum = -checkSum; 
  checkSum = checkSum & 0xFF;
    
  return checkSum;  
}

/*
 * Function: reset
 * --------------------------------
 * Clears the information in a request packet.
 * 
 * *req: Pointer to the RequestPacket to reset.
 */
void SerialComms::reset(SerialComms::RequestPacket *req) {
  req->destID = '0';
  req->op0 = '0';
  req->op1 = '0';
  req->channel = '0';
}

/*
 * Function: decodeRequest
 * --------------------------------
 * Converts the contents of a SerialBuffer to a RequestPacket
 * for processing. Currently copies ASCII chars directly from
 * buffer, but may become more complex if a binary protocol is 
 * adopted.
 * 
 * *string: String contents of buffer to be decoded and copied.
 * 
 * *req: RequestPacket to copy request into.
 */
void SerialComms::decodeRequest(char *string, SerialComms::RequestPacket *req) {
  req->destID = string[0];
  req->op0 = string[1];
  req->op1 = string[2];
  req->channel = string[3];
}

/*
 * Function: reset
 * --------------------------------
 * Clears the information in a response packet and frees
 * allocated data.
 * 
 * *resp: Pointer to the ResponsePacket to reset.
 */
void SerialComms::reset(SerialComms::ResponsePacket *resp) {
  resp->destID = '0';
  resp->dataLen = 0;

  if(resp->data != NULL) {
    free(resp->data);
  }
  
  resp->data = NULL;
}

/*
 * Function: isValidRequest
 * --------------------------------
 * Checks each member of a RequestPacket to verify that
 * it only contains meaningful codes.
 * 
 * *req: Pointer to the RequestPacket to check.
 */
int SerialComms::isValidRequest(SerialComms::RequestPacket *req) {
  if( !(req->op0 == 'T'  || req->op0 == 'S') ||
      !(req->op0 == 'R'  || req->op1 == 'L') ||
      !(req->channel >= '0' && req->channel <= '9')) {
    return 0;
  }      
  return 1;
}

/*
 * Function: handleRequest
 * --------------------------------
 * Processes a RequestPacket and returns a ResponsePacket 
 * if valid, else returns an error message.
 * 
 * *req: Pointer to the RequestPacket to handle.
 * 
 * *resp: Pointer to the ResponsePacket to send back.
 */
void SerialComms::handleRequest(SerialComms::RequestPacket *req, SerialComms::ResponsePacket *resp) {
  if(isValidRequest(req)) {
    processRequest(req, resp);
  }
  else {
    //Return error message
  }
}

