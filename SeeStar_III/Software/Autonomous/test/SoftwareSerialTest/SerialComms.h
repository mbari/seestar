#include "Arduino.h"

class SerialComms {
  private:
    Stream *_dev;

  public:
    struct ResponsePacket {
      char destID;
      unsigned short dataLen;
      char *data;
    }; 
    struct RequestPacket {
      char destID;
      char op0;
      char op1;
      char channel; 
    };
    SerialComms(Stream *dev): _dev(dev) {}  
    void gotoPos(int pos);
    int computeCheckSum(char *string);
    void takeSample(); 
      
    void reset(SerialComms::RequestPacket *req);
    void decodeRequest(char *buf, SerialComms::RequestPacket *req);
    
    SerialComms::ResponsePacket newResponsePacket();
    void reset(SerialComms::ResponsePacket *resp);
    
    int isValidRequest(SerialComms::RequestPacket *req);
    void handleRequest(SerialComms::RequestPacket *req, SerialComms::ResponsePacket *resp);
    
    int processRequest(SerialComms::RequestPacket *req, SerialComms::ResponsePacket *resp);
     
}
