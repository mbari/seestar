/* 
 Reads the SleepyPi RTC and prints out the time in local and UTC format.
 Assumes the RTC is set to local time. For testing purposes only.
 This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
 To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
*/
 
#include <SleepyPi2.h> 
#include <TimeLib.h>
#include <Timezone.h> 
 
//US Pacific Time Zone
TimeChangeRule usLOCALDT = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usLOCALST = {"PST", First, dowSunday, Nov, 2, -480};
Timezone myTZ(usLOCALDT, usLOCALST);

// reads the time from the RTC
time_t readt(void)
{ 
  DateTime t; 
  t = SleepyPi.readTime();
  return t.unixtime();
}

void setup(void)
{
  Serial.begin(9600); 
  Serial.println("Initializing"); 
  setSyncProvider(readt);   // pass the function to get the time from the RTC
  if(timeStatus()!= timeSet) 
      Serial.println("Unable to sync with the RTC");
  else
      Serial.println("RTC has set the system time");   
}

void loop(void)
{
    time_t utc, local; 
    Serial.println();
    local = now();
    utc = myTZ.toUTC(local);
    if (myTZ.utcIsDST(utc))
      printTime(local, usLOCALDT.abbrev);
    else
      printTime(local, usLOCALST.abbrev);
    printTime(utc, "UTC");
    delay(10000);
}

//Print time with time zone
void printTime(time_t t, char *tz)
{
    print2digits(hour(t));
    Serial.write(':');
    print2digits(minute(t));
    Serial.write(':');
    print2digits(second(t));
    Serial.print(' ');
    Serial.print(dayShortStr(weekday(t)));
    Serial.print(' ');
    print2digits(day(t));
    Serial.print(' ');
    Serial.print(monthShortStr(month(t)));
    Serial.print(' ');
    Serial.print(year(t));
    Serial.print(' ');
    Serial.print(tz);
    Serial.println();
} 

//Print an integer in "00" format (with leading zero).
void print2digits(int number) { 
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
} 
