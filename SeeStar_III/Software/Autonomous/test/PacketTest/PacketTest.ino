/* 
 Test that formats the system status packet that will be sent over the serial port 
 to the GUI for updates.  
 For testing purposes only.
 This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
 To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
*/
 
#include "SleepyPi2.h" 
#include <TimeLib.h>
#include <Timezone.h> 
 
//US Pacific Time Zone
TimeChangeRule usLOCALDT = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usLOCALST = {"PST", First, dowSunday, Nov, 2, -480};
Timezone myTZ(usLOCALDT, usLOCALST);

const char DATA_DELIMETER=';';
const char END_DELIMETER='~';
long int num_pics = 0; 

enum recordingMode {STILL=0, VIDEO, MIXED }; 
enum instrumentState {INITIALIZED=0, DELAYED, RUNNING, SHUTDOWN};
int mode=VIDEO; //recording mode
int state=DELAYED;
int duration=30; // duration for video recording
float minimum_voltage=12.0;
String sensor_clock="";
String sensor_log="Date,Time,Sensor,Reading\r\n1/1/0,2:32:5,S1,0\r\n1/1/0,2:32:12,S3,1\r\n1/1/0,2:32:27,S1,3\r\n1/1/0,2:32:28,S3,3";


// reads the time from the RTC
time_t readt(void)
{ 
  DateTime t; 
  t = SleepyPi.readTime();
  return t.unixtime();
}

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);  
  setSyncProvider(readt);   // pass the function to get the time from the RTC
  if(timeStatus()!= timeSet) 
      Serial.println("Unable to sync with the RTC");
  else
      Serial.println("RTC has set the system time");      
}

void loop() {  
  
  delay(10);  // voltage reading is artificially high if we don't delay first 
  float supply_voltage = SleepyPi.supplyVoltage();
  time_t utc, local, wakeup; 
  local = now();
  utc = myTZ.toUTC(local);
  wakeup = utc + 5*60;
  
  Serial.print(END_DELIMETER);
  if (myTZ.utcIsDST(utc))
    printISO8601Time(local, usLOCALDT.offset);
  else
    printISO8601Time(local, usLOCALST.offset);
  Serial.print(DATA_DELIMETER);  
  printISO8601Time(utc, 0);
  Serial.print(DATA_DELIMETER);  
  printISO8601Time(wakeup, 0);
  Serial.print(DATA_DELIMETER);  
  Serial.print(supply_voltage);
  Serial.print(DATA_DELIMETER);  
  Serial.print(minimum_voltage);
  Serial.print(DATA_DELIMETER);  
  Serial.print(num_pics);
  Serial.print(DATA_DELIMETER);  
  Serial.print(mode);
  Serial.print(DATA_DELIMETER);  
  Serial.print(state);
  Serial.print(DATA_DELIMETER);  
  Serial.print(duration);
  Serial.print(DATA_DELIMETER);
  // simulate clocks that are in sync
  char sensor_clock[20]; 
  sprintf(sensor_clock, " %02d:%02d:%02d", hour(utc), minute(utc), second(utc)); 
  Serial.print(sensor_clock); 
  Serial.print(DATA_DELIMETER);
  Serial.print(sensor_log);
  Serial.print(END_DELIMETER);  
  num_pics += 1;
  delay(10000); 
}

//Print time in ISO9601 format
void printISO8601Time(time_t t, int offset_minutes)
{
    char buf[50];
    int offset_hours = offset_minutes/60;
    sprintf(buf, " %4d-%02d-%02dT%02d:%02d:%02d%+03d:00", year(t), month(t), day(t), hour(t), minute(t), second(t), offset_hours); 
    Serial.print(buf);
}
