/*
  Turns on SeeStar LED module on for 3 seconds, then off for one second, repeatedly. For testing purposes only.
  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
  To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
 */
#include "LowPower.h"
#include "SleepyPi2.h"

int led_pin = 8;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(led_pin, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(led_pin, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(3000);               // wait for 3 seconds
  digitalWrite(led_pin, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);               // wait for 1 second
}
