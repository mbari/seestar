import processing.serial.*;
import java.util.Calendar;
import java.util.TimeZone;

/**************************************************************************
**** Test code for exercising the Tiny serial protocol with the SeeStar III
**** This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
**** To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
**************************************************************************/

Serial port; 
String COM = "/dev/cu.usbserial-FT9J9ERQ"; //<========Change this to your serial port
char PACKET_DELIMITER = '~';
char DATA_DELIMITER = ';'; 
boolean packetError = false; 
String data_packet = ""; 
enum packetState { WAITING, INITIALIZED }; 

Boolean VERBOSE = false; // change to true for more verbose output

packetState state = packetState.WAITING;

void setup()
{  
  size (800, 600);
  smooth();
  printArray(Serial.list());
  port = new Serial(this,COM, 9600); // starts the serial communication 
  port.buffer(3);
  //clear screen;
  background(0);  
}

void draw()
{
 
}

/**
Formats time command to reset the RTC to the current time
*/
void updateTime()
{
  Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  long secondsSinceEpoch = c.getTimeInMillis() / 1000L;
  //sendBinary('T', secondsSinceEpoch);
}

/**
Read serial packet between the PACKET_DELIMITER chars
*/
void serialEvent(Serial port) 
{   
  while (port.available() > 0) 
  {
    char inChar  = port.readChar();
    switch (state) {
      case WAITING: 
        if (inChar == PACKET_DELIMITER) {
          state = packetState.INITIALIZED;
          print("\n<=======" + state + "<============\n");
        }
        else  
          print(inChar);
      break;
      case INITIALIZED:
        if (inChar == PACKET_DELIMITER) {
          if (data_packet.length() >= 11)
          {
            print("<=======" + state + "<============Unpacking\n"); 
            parseData(data_packet); 
            data_packet = "";
          }
          state = packetState.WAITING;
          updateTime();
        }
        else 
        {  
          data_packet += inChar;   
          if (VERBOSE)
            print("<=====Received:" + inChar + "  packet length=============" + data_packet.length() + "\n");
        }
      break; 
    }                
  } 
    
}

/**
Parse data from a packet; returns true if the packet receives a single long value and a single int value
*/
Boolean parseData(String data_packet)
{   
  if (VERBOSE) {
    for ( int i = 0; i < data_packet.length(); i++)
      print("parsing data " + data_packet.charAt(i) + "\n");
  }
  if (data_packet.indexOf(DATA_DELIMITER) > 0) {
    String[] s = split(data_packet, DATA_DELIMITER);   
    if (s.length == 5) { 
      long valueLong = str2long(s[0]);
      int valueInt = str2int16(s[1]); 
      float floatVal = float(str2int16(s[2]))/1000.; 
      String stringVal = String.valueOf(s[3]); 
       print("Received long: " + valueLong + " int: " + valueInt + " float: " + floatVal + "\n");
       print ("String: " + stringVal  + " number of packets strings: " + s.length + "\n"); 
        
      return true;
    }
  }
  return false;
}
/**
Converts long to bytes
c*/
void sendBinary(char cmd, long s)
{   
  print("Writing " + PACKET_DELIMITER + int(s) + DATA_DELIMITER + PACKET_DELIMITER + '\n');
  port.write(PACKET_DELIMITER); 
  port.write(cmd); 
  port.write((char) (s & 0xFF));
  port.write((char) ((s >> 8) & 0xFF));
  port.write((char) ((s >> 16) & 0xFF));
  port.write((char) ((s >> 24) & 0xFF)); 
  port.write(PACKET_DELIMITER);  
}
/**
Converts string to long
*/
long str2long(String s)
{ 
  int int32 = s.charAt(3);
  int32 = ( int32 << 8) | (int) s.charAt(2);
  int32 = ( int32 << 8) | (int) s.charAt(1);
  int32 = ( int32 << 8) | (int) s.charAt(0);
  long f = (int32 & 0xFFFFFFFFL );
  return f;  
}

/**
Converts string to int16
*/
int str2int16(String s)
{  
  int int16 = (int) s.charAt(1);
  int16 = (int16 << 8) | (int) s.charAt(0);
  return int16;
} 
 