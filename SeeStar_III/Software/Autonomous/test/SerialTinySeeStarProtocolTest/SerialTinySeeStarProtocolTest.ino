/** 
 * Test basic serial commands to SeeStar. For testing purposes only.
 * This example sends a status packet update every TBD seconds
 * and listens for incoming requests to reset the clock or change
 * the wakeup frequency for the interval timer 
 */

#include <TinyLightProtocol.h>

#include "SleepyPi2.h" 
#include <TimeLib.h>
#include <Timezone.h>   
#include <DS1374RTC.h> 

//US Pacific Time Zone
TimeChangeRule usLOCALDT = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usLOCALST = {"PST", First, dowSunday, Nov, 2, -480};
Timezone myTZ(usLOCALDT, usLOCALST);

Tiny::ProtoLight  proto; 

char DATA_DELIMITER = ';'; 
char sensor_log[]="Date,Time,Sensor,Reading\r\n1/1/0,2:32:5,S1,0\r\n1/1/0,2:32:12,S3,1\r\n1/1/0,2:32:27,S1,3\r\n1/1/0,2:32:28,S3,3";

// reads the time from the RTC
time_t readt(void)
{ 
  DateTime t; 
  t = SleepyPi.readTime();
  return t.unixtime();
}

void setup() {
    /* No timeout, since we want non-blocking UART operations */
    Serial.setTimeout(0);
    /* Initialize serial protocol for test purposes */
    Serial.begin(9600); 
    /* Redirect all protocol communication to Serial0 UART */
    proto.beginToSerial();
    /* external time provider */      
    setSyncProvider(readt);   
    if(timeStatus()!= timeSet) 
        Serial.println("Unable to sync with the RTC");
    else
       Serial.println("RTC has set the system time");
    /* Print the time */   
    time_t utc, local;  
    utc = now();
    local= myTZ.toLocal(utc);    
    printISO8601Time("UTC time:", utc,0, "");
    if (myTZ.utcIsDST(utc)) 
      printISO8601Time("Local time:", local, usLOCALDT.offset, usLOCALDT.abbrev);
    else
      printISO8601Time("Local time:", local, usLOCALST.offset, usLOCALST.abbrev);
}
    
/* Specify buffer for packets to send and receive */
byte g_inBuf[32];
byte g_outBuf[32];

Tiny::Packet outPacket(g_outBuf, sizeof(g_outBuf));
Tiny::Packet inPacket(g_inBuf, sizeof(g_inBuf));

void loop()
  {       
    time_t utc, local;  
    utc = now();
    local= myTZ.toLocal(utc); 
      
    outPacket.clear();
    inPacket.clear();
    outPacket.put(utc);
    outPacket.put(DATA_DELIMITER); 
    outPacket.put(100);
    outPacket.put(DATA_DELIMITER); 
    outPacket.put((int) (12.698*1000)) ; // fake voltage 3 decimal places
    outPacket.put(DATA_DELIMITER);
    for (int i=0;i<sizeof(sensor_log); i++) // copy string array
      outPacket.put(sensor_log[i]);  
    outPacket.put(DATA_DELIMITER);   

    
    size_t s = outPacket.size();
    Serial.print("Packet size: ");
    Serial.print(s, DEC);
    Serial.println(); 
     
    /* Send packet over UART to other side */
    proto.write(outPacket);

    unsigned long start = millis();
    /* read answer from remote side */
    int len;
    do
    {
        len = proto.read(inPacket); 
        /* Wait 1000ms for any answer from remote side */
        if (millis() - start >= 1000)
        {
            break;
        }
    } while (len < 0);
    if (len > 0)
    {
        /* Process response from remote side */
        char cmd = inPacket.getChar();
        switch (cmd)
        {
          case 'T':
            uint32_t secs = inPacket.getUint32();
            setTime(secs); 
            DateTime dt(secs);
            SleepyPi.setTime(dt);        
        }
        inPacket.clear(); 
    }

   delay(5000);
}

/* Unused, but here for reference */
void pack_long_int(int j, unsigned long val)
{ 
  g_outBuf[j] = val & 0xFF;
  g_outBuf[j+1] = (val >> 8) & 0xFF; 
  g_outBuf[j+2] = (val >> 16) & 0xFF;
  g_outBuf[j+3] = (val >> 24) & 0xFF; 
  g_outBuf[j+4] = ';';
}

void pack_short_int(int index, int val)
{  
  g_outBuf[index] = val & 0xFF;  
  g_outBuf[index+1] = (val >> 8) & 0xFF;
  g_outBuf[index+2] = ';';
}


//Print time in ISO9601 format
void printISO8601Time(char *prefix, time_t t, int offset_minutes, char *abbrev)
{
    char buf[50];
    int offset_hours = offset_minutes/60;
    sprintf(buf, "%s %4d-%02d-%02dT%02d:%02d:%02d%+03d:00 %s\n", prefix, 
                                                              year(t), month(t), day(t), hour(t), minute(t), second(t), 
                                                              offset_hours, abbrev); 
    Serial.print(buf);
}
