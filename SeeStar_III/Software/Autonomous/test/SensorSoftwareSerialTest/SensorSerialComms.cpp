#ifndef SERIAL_COMMS_H
#define SERIAL_COMMS_H

#include "SensorSerialComms.h"
 
char SensorSerialComms::computeCheckSum(char *string)
{
  char checkSum = 0; 
  for (char *stringPtr = string; *stringPtr != '\0'; stringPtr++){
    checkSum += *stringPtr;
  }   
  return checkSum;  
}

void SensorSerialComms::debug(char *buffer, int length)
{
  if (_dev_debug) {
      for (int i=0; i < length; i++) {
          _dev_debug->write(buffer[i]); 
      }
    _dev_debug->write('\n');
  }
} 

int SensorSerialComms::sendCommand(char *cmd,
                            int years, int months, int days, 
                            int hours, int minutes, int seconds)
{
  char send_buffer[CMD_LENGTH]; 
  char receive_buffer[CMD_RESPONSE_LENGTH]; 
  
  int n = sprintf(send_buffer, "$%s%02d%02d%02d%02d%02d%02d",cmd,years,months,days,hours,minutes,seconds);
  int i = 0;

  debug(send_buffer, CMD_LENGTH);   
  char b =  computeCheckSum(send_buffer);
  send_buffer[CMD_LENGTH-1] = b; 
  debug(send_buffer, CMD_LENGTH);   
  
  for (i=0; i < CMD_LENGTH; i++)
    _dev->write(send_buffer[i]);    
  
  uint8_t d;
  uint8_t head = 0; 
  while (_dev->available()) {
    d = _dev->read();
    if (d != -1) {
      receive_buffer[head] = d;
      head = (head + 1) % CMD_RESPONSE_LENGTH; 
    }
    else {
      break;
    }
  }; // TODO: check if this return code generic across software/hardware serial libraries ?
   
  debug(receive_buffer,CMD_RESPONSE_LENGTH); 
  
  char *rc = NULL;
  rc = strstr(receive_buffer, "ACK");
  if (rc) {
    return 1;
  }
  else {
    return -1;
  }
} 

#endif  //SERIAL_COMMS_H
