#include "Arduino.h"
#include <SoftwareSerial.h>

#define TAKE_SAMPLE "TS"
#define REPORT_LOG  "RL" 

#define CMD_LENGTH 18
#define CMD_RESPONSE_LENGTH 6
#define START_FLAG '$'  // marks the start of a packet
#define END_FLAG '#'  // marks the end of a packet

/**
 * Serial Communication Class that encapsulates transmit/receive packets between the Sensor module and the Camera module
 */
class SensorSerialComms {
  private:
    Stream *_dev; 
    Stream *_dev_debug;
    
    /**
    * Checksum
    * Computes single byte checksum of string   
    */
    char computeCheckSum(char *string);

  public:
    /**
    * A constructor.
    * Uses pointer to generic Stream device to send/receive  
    */
    SensorSerialComms(Stream &dev) : _dev(&dev) {} ;
    
    /**
    * A constructor.
    * Uses pointer to generic Stream device to send/receive and debugging stream. Intended for 
    * debugging protocol only. 
    */
    SensorSerialComms(Stream &dev, Stream &dev_debug) : _dev(&dev), _dev_debug(&dev_debug) {} ;
    
    
    int sendCommand(char *cmd, int years, int months, int days, int hours, int minutes, int seconds);

  private:

  /**
   * Sends debugging information over serial port
   */
    void debug(char *buffer, int length);
};
