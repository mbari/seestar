/*
  Sends a take sample command to the Sensor Module and prints a return code. For testing purposes only.
  This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
  To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
*/

#include "SleepyPi2.h"
#include <SoftwareSerial.h>
#include <TimeLib.h> 
#include "SensorSerialComms.h"

SoftwareSerial ss(11,10); //Rx, Tx
SensorSerialComms coms(ss, Serial); 

void setup(){ 
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. 
  }  
  Serial.println("Initialized serial monitor to 9600 baud");
  ss.begin(119200); //119200
}

void loop()
{
  DateTime now = SleepyPi.readTime();
  Serial.println("TS");
  ss.write('$');
  int rc = coms.sendCommand(TAKE_SAMPLE, now.year(), now.month(), now.day(), now.hour(), now.minute(), now.second());
  //int rc = coms.sendCommand(TAKE_SAMPLE, 17, 1, 1, 5, 6, 7);
  Serial.print("Return code: ");
  Serial.println(rc);
  delay(2000);
}

