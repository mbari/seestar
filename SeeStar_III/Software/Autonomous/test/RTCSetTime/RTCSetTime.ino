/* 
 Sets the clock in response to serial port time message.
 To use, open the Arduino Serial Monitor and type the new time in the format:
 
 hour(24-format):minute:second month day year

 e.g.
 
  23:12:19  12 3 2017

 This will set the real time clock to the time 23:12:19 and the date 12-3-2017
 
 This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
 To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
*/

#include "SleepyPi2.h" 
#include <TimeLib.h>
#include <Wire.h>
#include <PCF8523.h>

PCF8523 rtc;
tmElements_t tm;

// reads the time from the RTC
time_t readt(void)
{ 
  DateTime t; 
  t = SleepyPi.readTime();
  return t.unixtime();
}

void setup()  {
  Serial.begin(9600);  
  setSyncProvider(readt);   // the function to get the time from the RTC
  if (timeStatus() != timeSet) 
     Serial.println("Unable to sync with the RTC");
  else
     Serial.println("RTC has set the system time");      
}

void loop()
{
  if (Serial.available()) {
    time_t secs = processSyncMessage();
    if (secs != 0) { 
      setTime(secs); 
      DateTime dt(secs);
      Serial.println("Initializing RTC...");
      rtc.setTime(dt);                  
    }
  }
  digitalClockDisplay();  
  delay(1000);
}

void digitalClockDisplay(){
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(month());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(" "); 
  Serial.print(year()); 
  Serial.println(); 
}

void printDigits(int digits)
{
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
} 

unsigned long processSyncMessage() 
{ 
  long seconds = 0L;
  char buffer[32]; 
  String s = Serial.readStringUntil('\n');
  s.toCharArray(buffer, 32);
  if(getTime(buffer) == true)
    seconds = makeTime(tm);  
  return seconds;
}

bool getTime(const char *str)
{
  int Hour, Min, Sec, Day, Year, Month; 

  if (sscanf(str, "%d:%d:%d %d %d %d",  &Hour, &Min, &Sec, &Month, &Day, &Year) != 6) 
    return false;
  tm.Hour = Hour;
  tm.Minute = Min;
  tm.Second = Sec;
  tm.Day = Day;
  tm.Month = Month;
  tm.Year = Year - 1970;
  return true;
}
 







