# SeeStar version III Autonomous Software GUI

This is a simple graphical interface for displaying status information
from the SeeStar system. It's based on the open source language/development tool
[Processing](https://processing.org/).

[![ Image link ](img/gui_screencapture.jpg)]

## Hardware prerequisites
 
- Sleepy Pi version 2 from [SpellFoundry] (https://github.com/SpellFoundry/)
- (optional) [External Programming Adapter] (https://spellfoundry.com/product/sleepy-pi-programming-adapter/)

## Software prerequisites
- Arduino development environment version 1.8.4 
- [Processing version 3.0](https://processing.org/) 

### Running

1. Install the [Arduino](https://www.arduino.cc/) development environment 
2. Install the [Processing](https://processing.org/) tool
3. Launch the Processing IDE. You should get an IDE like the following:
[![ Image link ](img/processing_IDE.jpg)]
4. Open the gui.pde file in the same directory as this README.me file
5. Click the green run button.
[![ Image link ](img/run_processing_IDE.jpg)]


