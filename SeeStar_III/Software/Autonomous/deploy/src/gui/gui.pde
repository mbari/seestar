import processing.serial.*;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**************************************************************************
**** Displays status information from the SeeStar III
**** This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License.
**** To view a copy of this license, see http://creativecommons.org/licenses/by-sa/4.0
**************************************************************************/

Serial port; 
String COM = "/dev/cu.usbserial-FT9J9ERQ"; //<========Change this to your serial port
int GRAPH_HEIGHT = 500; 
float VERSION = 1.0;
float VOLTAGE_OFFSET = 0.78;

// get linear mapping from volts to pixels the same height as the graph
float MIN_V = 12.0f;
float MAX_V = 25.0f;
float SLOPE = float(GRAPH_HEIGHT)/(MAX_V - MIN_V);
float INTERCEPT = -1. * SLOPE * MIN_V;

enum RecordingMode { STILL, VIDEO, MIXED }; 
enum State { INITIALIZED, DELAYED, RUNNING, SHUTDOWN };
enum packetState { WAITING, INITIALIZED }; 
char PACKET_DELIMITER = '~';
char DATA_DELIMITER = ';';
int MIN_PACKET_LENGTH = 38;
String data_packet = ""; 
Boolean VERBOSE = false; // change to true for more verbose output of serial handling
packetState state = packetState.WAITING;

String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
        "Sep", "Oct", "Nov", "Dec" };


int TXT_BGND_COLOR = 0; // text background color, gray value
PFont title_font;
PFont label_font;
 
class SeeStarData
{ 
    long localTime;
    long utcTime;
    long wakeupTime;
    RecordingMode mode;
    State state;
    int duration;
    float supplyVoltage;
    float minimumVoltage;
    int numPics;
    String sensorModuleLog;
    long sensorModuleTime;
}
int NUM_DATA = 11; //change to reflect the number of elements in the class SeeStarData

// globals
SeeStarData last = new SeeStarData();
SeeStarData current = new SeeStarData();
int blinkTime;
boolean blinkOn;
boolean recvPacket;
boolean packetError = false;
int blink_delay_msec = 500;

void setup()
{  
  print("Slope is " + SLOPE + " intercept is " + INTERCEPT + '\n');
  size (800, 600);
  smooth();
  printArray(Serial.list());
  port = new Serial(this,COM, 9600); // starts the serial communication 
  port.bufferUntil(PACKET_DELIMITER); 
  //clear screen;
  background(0);
  title_font = loadFont("AgencyFB-Reg-60.vlw"); 
  label_font = loadFont("ArialUnicodeMS-15.vlw");
  
  recvPacket = false;
  blinkTime = millis();
  blinkOn = true;
  
  /////////////////////////////////// 
  // draw supply voltage rectangle and levels
  rectMode(CORNERS); 
  stroke(255);
  fill(0,0,0);
  rect(50,GRAPH_HEIGHT,150,80); 
  fill(255, 255 , 255); 
  textFont(label_font, 14);  
  text(String.valueOf(MAX_V) + 'V', 10, 100);
  text(String.valueOf(MIN_V) + 'V', 10, GRAPH_HEIGHT);
  
  /////////////////////////////////// 
  // title
  textAlign(CENTER);
  fill(115, 210 , 250);
  textFont(title_font, 60);
  text("SeeStar", 400, 50);
  textFont(title_font, 20);
  text("Version:" + VERSION, 400, 70);
    
  ///////////////////////////////////
  //  text  
  fill(255, 255 , 255); 
  textFont(label_font, 20); 
  textAlign(RIGHT);
  text("Last packet time (local):", 380, 100); 
  text("Last packet time (UTC):", 380, 140); 
  text("Next wakeup time (UTC):", 380, 180); 
  text("Total pictures taken:", 380, 220); 
  text("Recording duration:", 380, 260);  
  text("Recording mode:", 380, 300); 
  text("Instrument state:", 380, 360); 
  text("Sensor time (UTC):", 380, 440);
  text("Sensor log:", 380, 480); 
  
  ///////////////////////////////////
  // Recording modes
  // draw 2 circles next to the text rectangle
  drawModeText(RecordingMode.STILL);
  fill(255, 255 , 255); 
  text("Video", 480, 330); 
  text("Still", 560, 330);  
  fill(128);
  ellipse(450, 300, 30, 30);
  ellipse(550, 300, 30, 30); 
  
  ///////////////////////////////////
  // Recording modes
  // draw 4 circles next to the text rectangle
  fill(255, 255 , 255); 
  text("Initialized", 480, 400); 
  text("Delayed", 580, 400);  
  text("Running", 680, 400);  
  text("Shutdown", 780, 400);  
  fill(128);
  ellipse(450, 360, 30, 30);
  ellipse(550, 360, 30, 30); 
  ellipse(650, 360, 30, 30); 
  ellipse(750, 360, 30, 30); 
  
  ///////////////////////////////////
  // Serial status  
  // create circle
  fill(0, 255, 0);
  ellipse(50, 50, 25, 25); 
  textAlign(RIGHT); 
  textFont(label_font, 14); 
  fill(255);
  text("Waiting for packet...", 200, 50); 
}

void draw()
{
  drawText();
  drawModes();
  drawState();
  drawVoltage(); 
  drawStatus();
} 

void drawStatus()
{
  if (recvPacket == true)  
  {
    // create circle
    fill(0, 255, 0);
    ellipse(50, 50, 25, 25);
    
    // clear text
    fill(TXT_BGND_COLOR);  
    rect(80, 30, 300, 60);
    
    if (millis() - 2000 > blinkTime) {
      recvPacket = false; 
      textAlign(LEFT); 
      textFont(label_font, 14); 
      fill(255);
      text("Waiting for packet...", 100, 50);
    }
    else 
    {
      textAlign(LEFT);
      textFont(label_font, 14); 
      fill(255);
      if (packetError == false)
        text("Received packet", 100, 50);
      else
        text("Packet error", 100, 50);
    }
  }
  else 
  {
    if (blinkOn) 
    { 
      fill(0);
      ellipse(50, 50, 25, 25);
      blink_delay_msec = 500;
    }
    else 
    {
      fill(128);
      ellipse(50, 50, 25, 25);
      blink_delay_msec = 500;
    }
    if (millis() - blink_delay_msec > blinkTime) 
    {
      blinkTime = millis();
      blinkOn = !blinkOn;
    }
  }
}
/**
Read serial packet between the PACKET_DELIMITER chars
*/
void serialEvent(Serial port) 
{   
  while (port.available() > 0) 
  {
    char inChar  = port.readChar();
    switch (state) {
      case WAITING: 
        if (inChar == PACKET_DELIMITER) {
          state = packetState.INITIALIZED;
          print("\n<=======" + state + "<============\n");
        }
        else  
          print(inChar);
        recvPacket = false;
      break;
      case INITIALIZED:
        if (inChar == PACKET_DELIMITER) {
          if (data_packet.length() >= MIN_PACKET_LENGTH ) {
            print("\n<=======" + state + "<============Unpacking\n"); 
            if (parseDataBinary(data_packet)) { 
              data_packet = "";
              recvPacket = true;
            }
          }
          state = packetState.WAITING;
          //updateTime();
        }
        else  {  
          data_packet += inChar;   
          if (VERBOSE)
            print("<=====Received:" + inChar + "  packet length=============" + data_packet.length() + "\n");
        }
      break; 
    }                
  }     
}

/**
Checks if the time is out of sync and resets it if so
*/
void updateTime()
{
  DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
  Calendar c = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  long secondsSinceEpoch1 = c.getTimeInMillis() / 1000L;
  
  Calendar utc = Calendar.getInstance();
  utc.setTimeInMillis(current.utcTime * 1000L); 
  
  Calendar local = Calendar.getInstance();
  local.setTimeInMillis(current.localTime * 1000L); 

  print("Computer : " + df.format(c.getTime()) +  "\n"); 
  print("RTC : " + df.format(utc.getTime()) + "\n");    
  
  long secondsSinceEpoch2 = utc.getTimeInMillis() / 1000L;
  long sec_diff = secondsSinceEpoch1 - secondsSinceEpoch2;
  
  print("Small time difference " + sec_diff + '\n');  
  if (abs(sec_diff) > 10 ) 
  {
    print("Large time difference " + sec_diff + " resetting clock now " + '\n');  
    sendBinary('T', secondsSinceEpoch1); 
  } 
}

/**
Scale the voltage to the graph height
*/
int scale_bar(float value)
{
  if (value > MIN_V && value <= MAX_V)
    return int((value*SLOPE) + INTERCEPT);  
  else
    return int((MIN_V*SLOPE) + INTERCEPT);  
}
/**
Converts string to long
*/
long str2long(String s)
{ 
  int int32 = s.charAt(3);
  int32 = ( int32 << 8) | (int) s.charAt(2);
  int32 = ( int32 << 8) | (int) s.charAt(1);
  int32 = ( int32 << 8) | (int) s.charAt(0);
  long f = (int32 & 0xFFFFFFFFL );
  return f;  
}

/**
Converts long to bytes
c*/
void sendBinary(char cmd, long s)
{   
  print("Writing " + PACKET_DELIMITER + int(s) + DATA_DELIMITER + PACKET_DELIMITER + '\n');
  port.write(PACKET_DELIMITER); 
  port.write(cmd); 
  port.write((char) (s & 0xFF));
  port.write((char) ((s >> 8) & 0xFF));
  port.write((char) ((s >> 16) & 0xFF));
  port.write((char) ((s >> 24) & 0xFF)); 
  port.write(PACKET_DELIMITER);  
}

/**
Converts string to int16
*/
int str2int16(String s)
{  
  int int16 = (int) s.charAt(1);
  int16 = (int16 << 8) | (int) s.charAt(0);
  return int16;
} 
/**
Parse data from a packet; returns true if the packet received all expected data
*/
Boolean parseDataBinary(String data_packet)
{
  print("Parsing " + data_packet + "\n");
  last.localTime           = current.localTime;
  last.utcTime             = current.utcTime;
  last.wakeupTime          = current.wakeupTime;
  last.numPics             = current.numPics;    
  last.mode                = current.mode;    
  last.state               = current.state;    
  last.duration            = current.duration;   
  last.sensorModuleLog    = current.sensorModuleLog;    
  last.sensorModuleTime   = current.sensorModuleTime;     
  String[] s = split(data_packet, DATA_DELIMITER);
   
  if (s.length == NUM_DATA + 1) {
    current.localTime        = str2long(s[0]);
    current.utcTime          = str2long(s[1]);
    current.wakeupTime       = str2long(s[2]);
    current.supplyVoltage    = float(str2int16(s[3]))/1000. + VOLTAGE_OFFSET;
    current.minimumVoltage   = float(str2int16(s[4]))/1000.;
    current.numPics          = str2int16(s[5]);
    current.mode             = RecordingMode.values()[str2int16(s[6])];
    current.state            = State.values()[str2int16(s[7])];
    current.duration         = str2int16(s[8]);
    current.sensorModuleTime = str2long(s[9]);
    current.sensorModuleLog  = String.valueOf(s[10]);
    print("RTC local time  = " + current.localTime + '\n');
    print("RTC utc time  = " + current.utcTime + '\n');
    print("RTC wakeup time  = " + current.wakeupTime + '\n');
    print("Supply voltage = " + current.supplyVoltage + '\n');
    print("Minimum voltage = " + current.minimumVoltage + '\n');

    switch (current.mode) {
      case STILL:
        print("Num pictures taken = " + current.numPics + '\n');
        break;
      case VIDEO:
        print("Num videos taken = " + current.numPics + '\n');
        print("Recording duration = " + current.duration + '\n');
        break;
      case MIXED:
        print("Num videos taken = " + current.numPics + '\n');
        print("Recording duration = " + current.duration + '\n');
        print("Num pictures taken = " + current.numPics + '\n'); 
        break;
      default:
        print("Error - invalid recording mode" + '\n');
        break;
    }
    
    switch (current.state) {
      case INITIALIZED:
        print("Mode = INITIALIZED" + '\n'); 
        break;
      case DELAYED:
        print("Mode = DELAYED" + '\n');  
        break;
      case RUNNING:
        print("Mode = RUNNING" + '\n');
        break;
      case SHUTDOWN:
        print("Mode = SHUTDOWN" + '\n');
        break;
      default:
        print("Error - invalid state" + '\n');
        break;
    }
    print("Sensor module time = " + current.sensorModuleTime + '\n');
    print("Sensor module log = " + current.sensorModuleLog.length() + '\n');
    return true;
  }
return false;
  
}

 

/**
Draw text fields
*/
void drawText()
{   
  if ((last.localTime != current.localTime) && (last.utcTime != current.utcTime)) {
    // clear text
    fill(TXT_BGND_COLOR);  
    rect(400, 100, 650, 80);
    rect(400, 140, 650, 120);
    rect(400, 180, 650, 160);
    rect(400, 220, 650, 200);
    rect(400, 260, 650, 240);
    rect(400, 420, 800, 600); 
  
    // write text
    fill(255, 255 , 255); 
    textFont(label_font, 20); 
    textAlign(LEFT);
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
    Calendar utc = Calendar.getInstance();
    utc.setTimeInMillis(current.utcTime * 1000L);
    
    Calendar local = Calendar.getInstance();
    local.setTimeInMillis(current.localTime * 1000L); 
    
    Calendar wakeup = Calendar.getInstance();
    wakeup.setTimeInMillis(current.wakeupTime * 1000L); 
    
    text(df.format(local.getTime()),400, 100); 
    text(df.format(utc.getTime()), 400, 140); 
    text(df.format(wakeup.getTime()),400, 180);
    text(current.numPics,400, 220);

    if (current.mode == RecordingMode.VIDEO ||
        current.mode == RecordingMode.STILL)
      text(current.duration, 400, 260);
 
    Calendar sensor = Calendar.getInstance();
    sensor.setTimeInMillis(current.wakeupTime * 1000L); 
    
    text(df.format(sensor.getTime()), 400, 440); 
    text(current.sensorModuleLog, 400, 480);
  }
}

/**
Draw mode text
*/
void drawModeText(RecordingMode mode) 
{    
  // clear the text box for the total and duration
  noStroke();
  fill(TXT_BGND_COLOR); 
  rect(170, 200, 390, 280); 
  
  switch (mode) {
    case VIDEO: 
      // write the text to reflect video not still
      noStroke();
      fill(255, 255 , 255); 
      textFont(label_font, 20); 
      textAlign(RIGHT); 
      text("Total videos taken:", 380, 220);
      text("Recording duration:" , 380, 260);
      break;
    case STILL: 
      // write the text to reflect still not video
      noStroke();
      fill(255, 255 , 255); 
      textFont(label_font, 20); 
      textAlign(RIGHT); 
      text("Total stills taken:", 380, 220);
      break;
    case MIXED:
      // write the text to reflect both
      noStroke();
      fill(255, 255 , 255); 
      textFont(label_font, 20); 
      textAlign(RIGHT); 
      text("Total stills taken:", 380, 220);
      text("Total videos taken:", 380, 220);
      text("Recording duration:" , 380, 260);
      break;
    default:
      print("Invalid recording mode" + '\n');
      break;
  } 
}

/**
Draw recording mode
*/
void drawModes()
{     
  if (last.mode != current.mode)
  {     
    drawModeText(current.mode);
    switch (current.mode) {
      case VIDEO:
        // color the VIDEO mode circle
        fill(0, 255, 0);
        ellipse(450, 300, 30, 30);
        fill(128);
        ellipse(550, 300, 30, 30);
        break;
      case STILL:
        // color the STILL mode circle
        fill(0, 255, 0);
        ellipse(550, 300, 30, 30);
        fill(128);
        ellipse(450, 300, 30, 30);
        break;
      case MIXED: 
        fill(0, 255, 0);
        ellipse(450, 300, 30, 30);
        ellipse(550, 300, 30, 30);
      default:
        break;
    }
  }
}

/**
Draw recording mode
*/
void drawState()
{ 
  if (last.state != current.state)
  {
    switch (current.state) {
      case INITIALIZED: 
        fill(128);
        ellipse(550, 360, 30, 30); 
        ellipse(650, 360, 30, 30); 
        ellipse(750, 360, 30, 30); 
        fill(0, 255, 0);
        ellipse(450, 360, 30, 30);
      break;
      case DELAYED:
        fill(128);
        ellipse(450, 360, 30, 30);
        ellipse(650, 360, 30, 30); 
        ellipse(750, 360, 30, 30); 
        fill(0, 255, 0);
        ellipse(550, 360, 30, 30); 
      break;
      case RUNNING:
        fill(128);
        ellipse(450, 360, 30, 30);
        ellipse(550, 360, 30, 30); 
        ellipse(750, 360, 30, 30);
        fill(0, 255, 0);
        ellipse(650, 360, 30, 30); 
      break;
      case SHUTDOWN:
        fill(128);
        ellipse(450, 360, 30, 30);
        ellipse(550, 360, 30, 30); 
        ellipse(650, 360, 30, 30); 
        fill(0, 255, 0);
        ellipse(750, 360, 30, 30); 
      break;
    }
  }
}

/**
Draws voltage bar and updates voltage text
*/
void drawVoltage()
{  
  // colored
  if (last.supplyVoltage != current.supplyVoltage)  {
    if (current.supplyVoltage > current.minimumVoltage + 1.0f)
      fill(0, 255 , 0);  // green
    else if (current.supplyVoltage > current.minimumVoltage + 0.5f)
      fill(253, 113 , 59);  // orange 
    else
      fill(255, 0 , 0);  // red
    rect(50,GRAPH_HEIGHT-scale_bar(current.supplyVoltage),150,GRAPH_HEIGHT);
  }
  //black
  else {
    fill(0, 0 , 0);
    rect(50,80,150, GRAPH_HEIGHT - scale_bar(current.supplyVoltage));
  }
  
  // clear text with same color as background
  noStroke();
  fill(TXT_BGND_COLOR);  
  rect(50, GRAPH_HEIGHT + 10, 280, GRAPH_HEIGHT+80);
  
  // write text
  noStroke();
  fill(255, 255 , 255); 
  textFont(label_font, 20); 
  textAlign(LEFT);
  text("Supply voltage:" + nf(current.supplyVoltage, 2, 3), 50, GRAPH_HEIGHT+30); 
  text("Minimum voltage:" + nf(current.minimumVoltage, 2, 3), 50, GRAPH_HEIGHT+60); 
}