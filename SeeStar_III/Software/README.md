# SeeStar version III Software

The third generation SeeStar system software is currently under development.
There are currently two versions envisioned for the SeeStar system: one for an autonomous, battery-powered deployment, 
and another for a tethered deployment. There is also an optional sensor module which can be used to interface to
oceanographic instruments. This sensor modules could provide ancillary measurements that might be relevant to a 
SeeStar camera system like temperature, depth, etc.

Currently, the Autonomous version is the furthest along in development. Instructions for installing that can be found here [Autonomous/](Autonomous)

[![ Image link ](img/block_diagram_autonomous.jpg)]
[![ Image link ](img/block_diagram_tethered.jpg)]
