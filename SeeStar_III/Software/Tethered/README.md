# SeeStar version III Software

The third generation SeeStar system software is currently under development.


## Hardware Prerequisites

- Raspberry Pi 2/3 model B
- Sleepy Pi version 2
- MicroSD card (pre-loaded with NOOBS recommended)
- HDMI cable
- USB keyboard and mouse
- Ethernet cable to connect to network or Raspberry Pi WiFi adapter

### Setup
Connect boards as detailed in your operations manual.   Power, display, keyboard, and MicroSD are minimally required.

### Sleepy Pi 2
Follow instructions on setting up Sleepy Pi 2 board here:
https://github.com/SpellFoundry/Sleepy-Pi-Setup

    ssh pi@raspberrypi
    
    pi@raspberrypi:$ cd /home/pi
    pi@raspberrypi:$ mkdir dev    
    pi@raspberrypi:$ cd dev    
    pi@raspberrypi:$ wget https://raw.githubusercontent.com/SpellFoundry/Sleepy-Pi-Setup/master/Sleepy-Pi-Setup.sh
    pi@raspberrypi:$ chmod +x Sleepy-Pi-Setup.sh
    pi@raspberrypi:$ sudo ./Sleepy-Pi-Setup.sh

    

### MicroSD card setup
Recommend the Raspbian operating system. This is a port of Debian jessie for the Raspberry Pi.
 
### NOOBS (sort of pre-baked)
Easiest with a microSD card pre-loaded with NOOBS. TODO: post images here of setup.

### Create your own (bake it yourself)

Recommend the Jessie Lite Raspberry image.
[![ Image link ](img/raspberry-pi-lite-screenshot.jpg)]

On Mac OSX installed Jessie Lite with the following commands

diskutil list

sudo diskutil unmount /dev/disk2s1
This will unmount, but not eject.

sudo dd bs=1m if=2017-01-11-raspbian-jessie-lite.img of=/dev/rdisk2
Copies the image - note this takes a few minutes. Don't kill this !  Wait until it's finished. Can press ctrl+T to see the progress.

sudo diskutil eject /dev/rdisk2
Eject the disk and copy the microSD to the RaspberryPI board

###Developer Notes
* On older version of Raspberry Pi, i2c is disabled; enable by adding to /etc/modules the line i2c-dev, and possible i2c-bcm2708, then reboot.
* SSH is not enabled by default in RPi3. Need to enable this for remote login. (see screencapture for this)


