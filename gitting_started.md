#### OPTIONAL IF ON WINDOWS: Install `msysgit` from [github](http://msysgit.github.io)

Otherwise: install git.
    
### Setting up your git environment

Set it up with your bitbucket info

    git config --global color.ui "auto"
    git config --global user.name "Your Name"
    git config --global user.email "your@email.address"
	git config --global core.editor nano

Use `cd` to move to the folder where you want the repo to be located on your computer.


#### OPTIONAL Create ssh keys
Associate them with your account on bitbucket, so you don't have to enter password every time

    ssh-keygen            #accept the default folder and enter a password
    cat ~/.ssh/id_rsa.pub
On a mac you can copy to clipboard using:

    cat ~/.ssh/id_rsa.pub | pbcopy
	
 * Copy the output of that command. Then go to your profile page, or this link:

    https://bitbucket.org/account/settings/ssh-keys/
 * Click on `Bitbucket settings` button in the upper right.
 * Scroll down to `SSH Keys` in the left column. 
 Choose `Add key` and paste your copied key into that window.
 * Remove your e-mail address from the end of the key...

### Clone the repository from the bitbucket server to make a local copy:

The menu in bitbucket will give you options:

![Bitbucket Menus](https://bitbucket.org/mbari/seestar/raw/master/images/bitbucketmenu.png "Bitbucket menus")
![Bitbucket Clone Command](https://bitbucket.org/mbari/seestar/raw/master/images/bitbucketclone.png "Bitbucket clone")


    git clone git@bitbucket.org:mbari/seestar.git

(It will create a `seestar` folder for you)

Edit and create documents in this folder using your normal tools.

When done with your edits:

     git fetch origin/master            # To get changes others have pushed
	 git add -A .                       # Add new files
	 git commit -am "Adding new file"   # Enter your changes, get ready to push them to the cloud

	 git pull                           # Before pushing, get other changes 
                                        # Also do this each time before starting to work

	 git push                           # May need to do `git push origin master` the first time
                                        # If there is a problem and you get into a weird screen,
                                        #   you might be in the `vi` editor. Do <ESC> :wq (four keystrokes) to exit. 
	 
	 git status                         # Show local differences

Find what is the status of remote machine:
    
    alias grd='git diff --stat origin/master' # see all changes, summarized into statistics

To track remote machine you might need to do 
    git branch --track origin/master
	
### Git handy commands
    git config --global core.editor "vim"
    
    git diff --stat HEAD~5 HEAD
	git diff HEAD~1 HEAD
